// Ionic Starter App

(function(){
    angular
        .module('ratemy', [
            'ionic',
            'ionic-material',
            'ry.global.controllers',
            'ry.personaldetails.controllers',
            'ry.menu.controllers',
            'ry.me.controllers',
            'ry.other.controllers',
            'ry.cat.controllers',
            'ry.login.controllers',
            'ratemy.services',
            'ngMessages',
            'ngAnimate',
            'ry.directives',
            'ratemy.directives'
        ])
        .run(runFunction)
        .config(configFunction);
    runFunction.$inject = ["$rootScope", "$ionicPlatform",
                           "$window"];
    configFunction.$inject = ["$stateProvider",
                              "$urlRouterProvider",
                              "$ionicConfigProvider",
                              "$sceDelegateProvider",
                              "$sceProvider",
                              "$compileProvider"];
    function runFunction($rootScope, $ionicPlatform, $window) {
        $ionicPlatform.ready(readyFunction);
        function readyFunction() {
            if($window.cordova && $window.cordova.plugins &&
               $window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if($window.StatusBar) {
                StatusBar.styleDefault();
            }
        }
    }
    function configFunction($stateProvider, $urlRouterProvider,$ionicConfigProvider,
                             $sceDelegateProvider, $sceProvider,
                             $compileProvider) {
        $sceProvider.enabled(true);
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://test.changala.fr/**',
            'file:**',
            'data:**'
        ]);
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);
        $ionicConfigProvider.tabs.position('bottom');
        $stateProvider
            .state('home', {
            url: '/home',
            templateUrl: 'html/home.html'
        })
            .state('login', {
            url: '/login',
            controller: 'loginCtrl',
            controllerAs: 'vm',
            templateUrl: 'html/login.html'
        })
            .state('tabs', {
            url: '/tabs',
            abstract: true,
            templateUrl: 'html/menu.html',
            resolve: {
                categories: function(services_ratemy, shdata, ryutils, $q){
                    var _categories = $q(function(resolve, reject){
                        resolve(shdata.categories || null);
                    });
                    var categories = _categories.then(function(cats){
                        if(cats === null){
                            return services_ratemy.categories().query({
                                username:shdata.get('username'),
                                token:shdata.get('token')
                            }, savecat, null).$promise;
                        }
                        return cats;
                    });
                    return categories;
                    function savecat(cats){
                        ryutils.toast('Login successful!');
                        shdata.set('categories', cats, false);
                        return cats;
                    }
                }
            },
        })
            .state('tabs.menu', {
            url: '/menu',

            views:{
                menuContent: {
                    controller: 'menuCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/menucat.html'
                }
            }
        })
            .state('tabs.self', {
            url: '/self/:user_id',
            params:{
                user_id:0,
            },
            views:{
                menuContent: {
                    controller: 'meCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/me.html'
                }
            }
        })
            .state('tabs.other', {
            url: '/other',
            views:{
                menuContent: {
                    controller: 'otherCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/other.html'
                }
            }
        })
            .state('tabs.meView', {
            url: '/other/:user_id',
            views:{
                menuContent: {
                    controller: 'meCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/me.html'
                }
            }
        })
            .state('tabs.cat', {
                url: '/cat/:type/:cat',
            params:{
                type:0
            },
            resolve: {
                slides: function(services_ratemy, shdata, ryutils, config,
                    $stateParams, $ImageCacheFactory, $rootScope){
                    var type = $stateParams.type;
                    var cat = $stateParams.cat || null;
                    var query = {};
                    query.username = shdata.username;
                    query.token = shdata.token;
                    if(cat !== null){
                        var catselected = shdata.categories[cat];
                        query.categories = catselected.id;
                    }
                    query.type = type;
                    query.offset = 0;
                    query.limit = 4;
                    var slides = services_ratemy.resources().query(query, saveslide);
                    return slides.$promise;
                    function saveslide(slides){
                        var imgs = $rootScope.map(slides, function(pic){return config.domain + pic.path;});
                        //return $ImageCacheFactory.Cache(imgs).then(function(){
                            return slides.$promise;
                        //});
                    }
                }
            },
            views:{
                menuContent: {
                    controller: 'catCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/cat.html'
                },
                fabContent: {
                    template: ['<button id="fab-gallery" class="button button-fab button-fab-top-right expanded button-energized-900 drop"',
                        'ry-type="type"',
                        'ry-cat="cat"',
                        'ry-insert-resource >',
                        '<i class="icon ion-camera"></i>',
                    '</button>'].join(''),
                    controller: function ($scope, $timeout, $stateParams, shdata) {
                        $scope.type = $stateParams.type;
                        if($stateParams.cat){
                            $scope.cat = shdata.categories[$stateParams.cat] || null;
                        }
                        $timeout(function () {
                            document.getElementById('fab-gallery').classList.toggle('on');
                        }, 600);
                    }
                }
            }
            })
            .state('tabs.resource', {
            url: '/resource',
            views:{
                menuContent: {
                    templateUrl: 'html/resourceView.html'
                }

            }
        })
        .state('tabs.details', {
            url: '/config',
            views:{
                menuContent: {
                    controller: 'personalDetailsCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/personalDetails.html'
                }
            }
        });
        $urlRouterProvider.otherwise("/home");
    }
})();

(function(){
    angular
    .module('ry.cat.controllers',
        ['ionic', 'ionic-material', 'ratemy.services',
            'ngMessages', 'ngAnimate', 'ui.router', 'ionicLazyLoad',
        'ngCordova', 'angular-underscore', 'ionic.ion.imageCacheFactory'])
    .controller("catCtrl", catCtrl);

    function catCtrl($scope, $stateParams, $cordovaCapture,
                      services_ratemy, shdata,
                      ryutils, ionicMaterialInk, ionicMaterialMotion,
                      $ionicSlideBoxDelegate, $cordovaCamera,
                      $ionicScrollDelegate, $ionicPopup,
                      $timeout, $ionicModal, $ionicActionSheet,
                      $cordovaMedia, $ionicPlatform, $ImageCacheFactory,
                      $cordovaFile, $interval, $sce, $window, slides, config){
        var vm = this;
        vm.cats = shdata.categories;
        var username = shdata.username;
        var token = shdata.token;
        vm.type = $stateParams.type;
        vm.config = config;
        vm.slides = slides;
        vm.reachEnd = reachEnd;
        vm.resourceMore = true;
        vm.filterpopup = false;
        vm.getactiveslides = getactiveslides;

        activate();

        /*******/
        function activate(){
            ryutils.showHeader();
            ryutils.clearFabs();
            ryutils.setExpanded(true);
            ryutils.setHeaderFab(false);
            ionicMaterialInk.displayEffect();
            $scope.each(vm.cats, function(cat){
                cat.show = true;
            });
            $scope.$watch("vm.slider", function(newval, old){
                if(newval != old){
                    vm.slider.on('ReachEnd', reachEnd);
                }

            });
        }
        function getactiveslides(slides){
            var tmp = $scope.filter(vm.cats,
                                    function(cat){
                                        return cat.show;
                                    });
            vm.catidarray = $scope.map(tmp, function(cat){return cat.id;});
            tmp = $scope.filter(slides,
                function(slide){
                    return $scope.contains(vm.catidarray,slide.category);
                });
            return tmp;
        }
        function extractImages(data){
            var imgs = $scope.map(data, function(pic){return vm.config.domain + pic.path;});
            //$ImageCacheFactory.Cache(imgs).finally(function(){
                vm.slides = vm.slides.concat(data);
                $ionicScrollDelegate.resize();
                if(data.length <= 0 || vm.slides.length <= 0){
                    ryutils.toast('End of the list!');
                    vm.resourceMore = false;
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
                ionicMaterialInk.displayEffect();
           // });
        }

        function reachEnd(){
            var index = vm.slides.length;
            var query = {};
            query.username = username;
            query.token = token;
            if($stateParams.cat){
                var catselected = shdata.categories[$stateParams.cat];
                query["categories"] = catselected.id;
            }
            query.offset = index + 1;
            query.limit = 6;
            query.type = vm.type;
            return services_ratemy.resources().query(query,
                extractImages,ryutils.err);
        }

    }
})();

(function(){
    angular
    .module('ry.directives',
        ['ionic', 'ionic-material', 'ngAnimate', 'angular-underscore',
            'ionicLazyLoad', 'ngCordova', 'ratemy.services', 'ratemy.config',
        'ionMdInput', 'ngMessages'])
    .directive('ryComments', ryComments);
    ryComments.$inject = ["$window", "ryutils", "$ionicModal",
        "services_ratemy", "shdata", '$rootScope', "config", "$ionicPopup",
        "$timeout"];
    ryCommentsCtrl.$inject = ["$scope", "$element", "$attrs", "services_ratemy",
    "shdata", "ryutils", "$timeout", "$rootScope", "$ionicPopup", "config",
    "$ionicModal", "$q"];
    function ryCommentsCtrl(scope, element, attrs, services_ratemy, shdata,
    ryutils, $timeout, rootScope, $ionicPopup, config, $ionicModal, $q){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.me = shdata.me;
        vm.getcomments = getcomments;
        vm.config = config;
        vm.commentsMore = true;
        vm.edit = false;
        vm.comments = [];
        vm.deletecomment = deletecomment;
        vm.postcomment = postcomment;
        vm.votecomments = votecomments;
        vm.getpict = ryutils.getpict;
        vm._saved = {
            value: false,
            text:"Saved",
            color:"badge-stable",
            handle:null
        };

        activate();
        element.on("click", function(){
            vm.commentsmodal.show();
            scope.$apply();
        });
        /****/
        function activate(){
            vm.mycomment = getmycomment();
            getcomments();
            scope.$watch('vm.mycomment.id', function(newval){
                if(typeof newval === 'undefined' || newval === null){
                    vm.mycomment.css = "black";
                }
                else{
                    vm.mycomment.css = "royal";
                }
            });
            $ionicModal.fromTemplateUrl('html/ry-comments.html', {
                scope: scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                vm.commentsmodal = modal;
                vm.commentsmodal.scope.vm = vm;
            });
            scope.$on('$destroy', function() {
                vm.commentsmodal.remove();
            });
        }
        function deletecomment(){
            var alertPopup = $ionicPopup.confirm({
                title: 'Are you sure?',
                template: 'Once deleted the comment and its notes cannot be retrieved'
            });
            alertPopup.then(function(res) {
                if(res) {
                    var query = {};
                    query.token = token;
                    query.username = username;
                    query.resource_id = vm.slide.id;
                    vm.mycomment.$delete(query, refreshcomments);
                } else {
                }
            });
        }
        function postcomment() {
            vm.mycomment.username = username;
            vm.mycomment.token = token;
            vm.mycomment.resource_id = vm.slide.id;
            vm.mycomment.$save(_postsave);
            vm.edit = false;
        }
        function _postsave(){
            vm.getcomments();
            vm._saved.value = true;
            vm._saved.text = "Saved";
            vm._saved.color = "badge-balanced";
            $timeout.cancel(vm._saved.handle);
            vm._saved.handle = $timeout(function(){
                vm._saved.value = false;
            }, 2000);

        }
        function votecomments(text, note){
            if(note == text.my_note){return;}
            var comments_vote = services_ratemy.comments_vote();
            var vote = new comments_vote();
            vote.token = token;
            vote.username = username;
            vote.user_id = vm.me.id;
            vote.comments_id = text.id;
            vote.note = note;
            vote.$save();
            text.my_note = parseInt(note, 10);
            text.pos_note = parseInt(text.pos_note, 10);
            text.neg_note = parseInt(text.neg_note, 10);
            text.pos_note = Math.max(0, text.pos_note + note);
            text.neg_note = Math.max(0, text.neg_note - note);

        }
        function getmycomment() {
            var query = {};
            query.offset = 0;
            query.limit = 1;
            query.token = token;
            query.username = username;
            query.user_id = vm.me.id;
            query.resource_id = vm.slide.id;
            query.arrayexpected = false;
            return services_ratemy.comments().get(query);
        }
        function refreshcomments(){
            vm.comments.length = 0;
            getcomments();
        }
        function getcomments(){
            var query = {};
            query.offset = vm.comments.length;
            query.limit = 20;
            query.token = token;
            query.username = username;
            query.resource_id = vm.slide.id;
            query.arrayexpected = true;
            return services_ratemy.comments().query(query, _commentsextract);
        }
        function _commentsextract(data){
            if(data.length === 0 && vm.comments.length > 0){
                vm.commentsMore = false;
                ryutils.toast('End of comments');
            }
            else{
                vm.commentsMore = true;
                vm.comments = vm.comments.concat(data);
                var queryvote = {};
                queryvote.offset = 0;
                queryvote.limit = 20;
                queryvote.token = token;
                queryvote.username = username;
                queryvote.user_id = vm.me.id;
                queryvote.arrayexpected = true;
                queryvote.comments_id = rootScope.map(data, function(comment){
                    return comment.id;
                });
                services_ratemy.comments_vote().query(queryvote, function(data){
                    vm.votes = data;
                    rootScope.each(vm.comments, addmynote);
                });
            }
            $timeout(function(){
                scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        function addmynote(comment){
            var predicate = {"comments_id": comment.id};
            var myvote = rootScope.findWhere(vm.votes, predicate) || {};
            comment.my_note = myvote.note || 0;
        }
    }
    function ryComments($window, ryutils, $ionicModal, services_ratemy,
        shdata, $rootScope, config, $ionicPopup, $timeout) {
            var directive = {
                restrict: 'A',
                templateNamespace: 'html',
                scope: {
                    slide: '=',
                    mycomment: '='
                },
                controller: ryCommentsCtrl,
                bindToController: true,
                controllerAs: "vm",
                link: ryCommentsLink
            };
            return directive;
            function ryCommentsLink(scope, element, attrs, ctrl){
            }
    }
})();

var app = angular.module('ratemy.config', []);

app.constant('configstatic', {
    //'domain': 'http://192.168.1.21/ratemy',
    'domain': 'http://ratemy.changala.fr',
    'default_picture': "/profiles/default_profile.png",
    'getcatclasstype': function(type){
        var types = ['ion-camera', 'ion-volume-high', 'ion-videocamera',
            'ion-document-text'];
        if(type >= 0 && type < types.length){
            return types[type];
        }
        return types[3];
    },

});

(function(){
    var app = angular.module('ratemy.directives', ['ionic', 'ngAnimate',
                                                   'angular-underscore',
                                                   'ngCordova',
                                                   'ratemy.services', 'ratemy.config']);



    app.directive('ryRecordSound', function ($interval, $timeout, $cordovaMedia,
                                              $cordovaFile, $q) {
        return {
            templateUrl: 'html/ry-record-sound.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                ryResource :'='
            },
            controller:['$scope', '$element', '$attrs', '$rootScope',
                        function($scope, $element, $attrs, $rootScope) {
                            $scope.currenttime = 0;
                            $scope.maxtime = 5;
                            $scope.isplaying = false;
                            $scope.isrecording = false;
                            $scope.recorddisplay = {false:'ion-mic-a balanced',
                                                    true:'ion-stop assertive'};
                            $scope.playdisplay = {false:'ion-play balanced',
                                                  true:'ion-stop assertive'};
                            $scope.updaterecordtime = function(_time){
                                $scope.currenttime = Math.round(_time*10)/10;
                            };
                            var tmp;
                            var tmp2;
                            var saveDuration = $scope.maxtime;
                            $scope.asyncGreet = function() {
                                var defer = $q.defer();
                                var _time = 0.0;
                                var srcaudio = cordova.file.externalDataDirectory+"/ratemy.amr";
                                var audio = $cordovaMedia.newMedia(srcaudio);
                                tmp = $interval(function(){
                                    _time += 0.1;
                                    defer.notify(_time);
                                    if(_time >= $scope.maxtime || !$scope.isrecording){
                                        $scope.isrecording = false;
                                        audio.stopRecord();
                                        audio.release();
                                        saveDuration = Math.round(_time*100)/100;
                                        $scope.maxtime = saveDuration;
                                        defer.resolve(srcaudio);
                                        $interval.cancel(tmp);
                                    }
                                }, 100);
                                $timeout(function(){
                                    $scope.isrecording = true;
                                    audio.startRecord();
                                });
                                return defer.promise;
                            };
                            $scope.asyncPlay = function() {
                                var defer = $q.defer();
                                var _time = 0.0;
                                var srcaudio = cordova.file.externalDataDirectory+"/ratemy.amr";
                                var audio = $cordovaMedia.newMedia(srcaudio);
                                tmp = $interval(function(){
                                    _time += 0.1;
                                    defer.notify(_time);
                                    $scope.maxtime = saveDuration;
                                    if(_time >= saveDuration || !$scope.isplaying){
                                        $scope.isplaying = false;
                                        audio.stop();
                                        audio.release();
                                        defer.resolve(srcaudio);
                                        $interval.cancel(tmp);
                                    }
                                }, 100);
                                $timeout(function(){
                                    $scope.isplaying = true;
                                    audio.play();
                                });
                                return defer.promise;
                            };
                            $scope.play = function () {
                                if(!$scope.isplaying){
                                    $scope.asyncPlay().then(function(filepath){
                                        $scope.currenttime = 0;
                                    }, null, $scope.updaterecordtime);
                                }
                                else{
                                    $scope.isplaying = false;
                                }
                            };
                            $scope.record = function () {
                                if(!$scope.isrecording){
                                    $scope.asyncGreet().then(function(filepath){
                                        $scope.currenttime = 0;
                                        $scope.ryResource = filepath;
                                    }, null, $scope.updaterecordtime);
                                }
                                else{
                                    $scope.isrecording = false;
                                }
                            };
                        }],
            link: function (scope, iElement, iAttrs, ctrl) {
            }
        };
    });
})();

(function(){
    angular.module('ry.directives')
    .directive('ryFavorite', ryFavorite);

    ryFavoriteCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', "$timeout"];
    function ryFavoriteCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.addOrDeleteFavorites = addOrDeleteFavorites;

        /** init **/
       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       element.on("click", function(){
           vm.addOrDeleteFavorites();
           scope.$apply();
       });

        function addOrDeleteFavorites(){
            if(angular.isUndefined(vm.myfavorite.id) || vm.myfavorite.id == 0){
                vm.myfavorite.resource_id = vm.slide.id;
                vm.myfavorite.username = username;
                vm.myfavorite.token = token;
                vm.myfavorite.$save();
            }else{
                vm.myfavorite.$delete({username:username,
                    token:token,
                    resource_id:vm.slide.id},function(data){
                        vm.myfavorite = data;
                    });
            }
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myfavorite = services_ratemy.favorites().get(query);
            scope.$watch('vm.myfavorite.id', function(newval){
                if(angular.isUndefined(newval) || newval == 0){
                    vm.myfavorite.css = "dark";
                }else{
                    vm.myfavorite.css = "balanced";
                }

            });
        }
    }
    ryFavorite.$inject = ['services_ratemy', 'shdata', '$ionicPopup',
    'config'];
    function ryFavorite(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button icon button-icon icon-left ion-heart"',
                'ng-class="vm.myfavorite.css"',
                'ng-click="vm.addOrDeleteFavorites()">',
                '</a>'
            ].join(''),*/
            restrict: 'AE',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myfavorite: '='
            },
            controller:ryFavoriteCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();

(function(){
    angular.module('ry.directives').directive('ryFilter', ryFilter);

    ryFilterCtrl.$inject = ["$scope", "$element", "$attrs", "services_ratemy"];
    function ryFilterCtrl($scope, $element, $attrs, services_ratemy) {
        var vm = this;
        vm.filterListSelected = [];
        if(vm.filterType == 'unique'){
            vm.uniqueType = true;
            vm.isActive = vm.filterList[0];
            vm.setClass = function(num) {
                vm.isActive = vm.filterList[num];
                vm.filterListSelected = vm.filterList[num].name;
            }
            $scope.$watch('vm.filterListSelected',function(selected){
                vm.filterSelected = selected;
            });
        }
        if(vm.filterType == 'multiple'){
            vm.uniqueType = false;
            vm.selectedFilter = function(num){
                vm.filterListSelected.push(vm.filterList[num]);
                for(var i = 0; i< vm.filterListSelected.length;i++)
                {
                    if(vm.filterListSelected[i].show == false)
                    {
                        vm.filterListSelected.splice(i,1);
                    }
                }

            }


            vm.filterSelected = vm.filterListSelected;

        }

    }
    function ryFilter() {
        return {
            templateUrl: 'html/ry-filter.html',
            restrict: 'E',
            templateNamespace: 'html',
            bindToController: true,
            controllerAs: "vm",
            scope: {
                filterType:'@',
                filterList:'=',
                filterSelected:'='
            },
            controller:ryFilterCtrl,
            link: function (scope, element, attrs, ctrl) {

            }
        };
    }
})();

(function(){
    angular
    .module('ry.global.controllers', ['ionic', 'ratemy.services',
            'ngMessages', 'ngAnimate', 'ui.router',
            'ngCordova', 'angular-underscore'])
    .controller("globalCtrl", globalCtrl);

    globalCtrl.$inject = ["$scope", "$state", "$log", "shdata", "config"]; 

    function globalCtrl($scope, $state, $log, shdata,
                        config){
        var vm = this;
        vm.config = config;
        vm._err = function(err){
            $log.info(err);
        };
    }

})();

(function(){
    angular.module('ry.directives')
    .directive('ryInsertResource', ryInsertResource);

    ryInsertResource.$inject = ['$cordovaFile',
        '$cordovaCapture', '$ionicPlatform', '$ionicModal', 'shdata',
        '$cordovaToast', 'config', 'services_ratemy', '$parse'];
    ryInsertResourceCtrl.$inject = ["$scope", "$element", "$attrs",
        '$cordovaFile', '$cordovaCapture', '$ionicPlatform', '$ionicModal',
        'shdata', '$cordovaToast', 'config', 'services_ratemy', '$parse',
        'ryutils', "$cordovaImagePicker"
    ];
    function ryInsertResourceCtrl(scope, element, attrs, $cordovaFile,
        $cordovaCapture, $ionicPlatform, $ionicModal, shdata, $cordovaToast,
        config, services_ratemy, $parse, ryutils, $cordovaImagePicker) {
            var vm = this;
            var fn = $parse(attrs['ng-click']);
            vm.uploading = false;
            vm.prog = "Upload";
            vm.categories = shdata.categories;
            vm.register_logo= [
                'ion-camera',
                'ion-mic-c',
                'ion-videocamera',
                'ion-compose'
            ];
            var username = shdata.get('username');
            var token = shdata.token;

            vm.upload = upload;
            vm.cat = vm.ryCat;
            vm.captureResource = captureResource;
            vm.selectResource = selectResource;
            vm.getimg = getimg;

            activate();

            function activate(){
                element.bind('click', function(event) {

                    //running the function
                    scope.$apply(function() {
                        fn(scope, {$event:event});
                        vm.modal.show();
                    });
                });
                $ionicPlatform.ready(function(){
                    $ionicModal.fromTemplateUrl('html/add_change_resource.html', {
                        scope: scope,
                        hardwareBackButtonClose: true,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        vm.modal = modal;
                    });
                });
                scope.$on('$destroy', function() {
                    vm.modal.remove();
                });
            }
            function selectImage() {
                var options = {
                    maximumImagesCount:1,
                    width: 0,
                    height:0
                };
                $cordovaImagePicker.getPictures(options).then(function(imgs) {
                    vm.img_path = imgs[0];
                });
            }
            function selectResource(){
                var capt = [
                    selectImage,
                    null,
                    null
                ];
                if(vm.ryType < 0 && vm.ryType > 2){
                    return selectImage;
                }
                capt[vm.ryType]();
            }
            function captureResource(){
                var capt = [
                    captureImage,
                    null,
                    null
                    /*scope.captureAudio,
                scope.captureVideo*/
                ];
                if(vm.ryType < 0 && vm.ryType > 2){
                    return captureImage;
                }
                capt[vm.ryType]();
            }
            function captureImage() {
                var options = { limit: 1 };
                $cordovaCapture.captureImage(options).then(function(imgs) {
                    var img = imgs[0];
                    vm.img_path = img.fullPath;
                });
            }
            function upload() {
                var params = {
                    username: username,
                    token: token,
                    name: vm.name,
                    description: vm.descr,
                    type: vm.ryType,
                    categories: vm.cat.id
                };
                vm.uploading = true;
                /*if(scope.ryType == 3){
                    vm.img_path = captureText(vm.img_path);
                }*/
                services_ratemy.images.insert(params,
                    vm.img_path, _success, _err, _prog);
            }
            function _success(data){
                ryutils.toast(vm.name + ' uploaded!');
                vm.uploading = false;
                vm.modal.hide();
            }
            function _err(err){
                ryutils.toast('Failed to upload!');
                vm.uploading = false;
                vm.prog = "Upload";
                console.log(JSON.stringify(err));
            }
            function _prog(progress){
                var _val = progress.loaded / progress.total * 100;
                vm.prog = Math.floor(_val) + "%";
            }

            function getimg(){
                var style = {
                    height: "300px",
                    'background-size': 'cover',
                    'background-position': 'center center',
                    'background-image': "url('" + vm.img_path + "')"
                };
                return style;
            }
        }
    function ryInsertResource($cordovaFile, $cordovaCapture,
        $ionicPlatform, $ionicModal, shdata, $cordovaToast, config,
        services_ratemy, $parse) {
            return {
                restrict: 'A',
                templateNamespace: 'html',
                scope: {
                    ryCat: '=',
                    ryType: '='
                },
                controller:ryInsertResourceCtrl,
                bindToController: true,
                controllerAs: "vm",
                link: function (scope, element, attrs, ctrl) {
                }
            };
        }
})();

(function(){
    angular
        .module('ry.login.controllers',
                ['ionic', 'ratemy.services',
                 'ngMessages', 'ngAnimate', 'ui.router',
                 'ngCordova', 'angular-underscore'])

        .controller("loginCtrl", loginCtrl);

    function loginCtrl ($scope, $ionicPopup, $state,
                         services_ratemy, config,
                         shdata, ryutils, $q){
        var vm = this;

        vm.username = shdata.get('username', "");
        vm.pass1 = shdata.get("password", "");
        vm.submit = submit;

        activate();

        /***/
        function submit() {
            if(vm.iscreation){
                return register();
            }
            return login();
        }
        function errlogin(err){
            ryutils.toast('Login failed, check your credentials!');
        }
        function postlogin(data){
            shdata.token = data.token;
            shdata.set('username', vm.username);
            shdata.set('password', vm.pass1);
            var user = services_ratemy.users().get({username:vm.username,
                token:data.token,
                id:data.id},function(remainingdata){
                    shdata.me = $scope.extend(data, remainingdata);
                });
            var config = services_ratemy.config().query({username:vm.username,
                token:data.token},
                function(configdata){
                    $scope.forEach(configdata, function(row){
                        config[row.name] = JSON.parse(row.value);
                    });
                });
            $q.all([user.$promise, config.$promise]).then(function(){
                $state.go('tabs.menu');
            });
        }
        function login() {
            // Start showing the progress
            // Do the call to a service using $http or directly do the call here
            var hashed = services_ratemy.hash(vm.pass1);
            return services_ratemy.users().login({username:vm.username, pass1:hashed},
                postlogin, errlogin);
        }
        function register() {
            var hashed1 = services_ratemy.hash(vm.pass1);
            var hashed2 = services_ratemy.hash(vm.pass2);
            return services_ratemy.users().register({username:vm.username,
                pass1:hashed1,
                pass2:hashed2},
                postlogin, function(data){
                    $ionicPopup.alert({
                        title: 'Registration failed!',
                        template: 'Please check your credentials!'
                    });
                });
        }
        function activate(){
            if(vm.username != '' && vm.pass1 != ''){
                login();
            }
        }
    }

})();

(function(){
        angular
        .module('ry.me.controllers',
            ['ionic', 'ratemy.services', 'ngMessages', 'ngAnimate',
                'ui.router', 'ngCordova', 'angular-underscore'])
        .controller("meCtrl", meCtrl);
    meCtrl.$inject = ["$scope", "$state", "shdata", "services_ratemy",
    "config", "$ionicPopup", "$ionicModal", "$stateParams", "ryutils",
    "$timeout", "ionicMaterialInk", "ionicMaterialMotion"];
    function meCtrl($scope, $state, shdata, services_ratemy,
                     config, $ionicPopup, $ionicModal,$stateParams,
                     ryutils, $timeout, ionicMaterialInk, ionicMaterialMotion){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.get('token');
        vm.config = config;
        vm.me = shdata.me;
        vm.categories = shdata.categories;
        vm.viewResources = viewResources;
        vm.getmoreresources = getmoreresources;
        vm.select = select;
        vm.selection = {curr:"pictures"};
        vm.selection.pictures = {
            service:services_ratemy.resources(),
            title:"User's Pictures:",
            csstitle:"ion-images"
        };
        vm.selection.favorites = {
            service:services_ratemy.favorites(),
            title:"User's Favorites:",
            csstitle:"ion-heart"
        };
        vm.resetpictures = resetpictures;
        vm.getcatfromid = getcatfromid;
        vm.iscurrentuser = iscurrentuser;
        vm.showResource = showResource;
        vm.getpict = ryutils.getpict;

        /**init ctrl*/
        activate();
        // self information
        function getuser(user_id){
            var query = {username:username,
                token:token,
                id:user_id,
                offset:0,
                limit:1
            };
            var user = services_ratemy.users().get(query);
            return user;
        }
        function activate(){
            var id = $stateParams.user_id;
            ryutils.showHeader();
            ryutils.clearFabs();
            ryutils.setExpanded(true);
            ryutils.setHeaderFab(false);
            ionicMaterialInk.displayEffect();
            $timeout(function() {
                ionicMaterialMotion.slideUp({
                    selector: '.slide-up'
                });
            }, 300);

            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 3000
                });
            }, 700);

            resetpictures("favorites");
            resetpictures("pictures");
            if(id == 0){
                vm.user = shdata.me;
                vm.user.user_id = vm.user.id;
            }
            else{
                vm.user = getuser(id);
            }
            var tmp = $scope.filter(vm.categories,
                                    function(cat){return !cat.deleted;});
            vm.catidarray = $scope.map(tmp, function(cat){return cat.id;});
            vm.pict = {
                'background-image': "url('" + vm.getpict(vm.user.picture || vm.config.default_picture) +"')"
        };
        }

        function showResource(pic){
            vm.slide = pic;
            if(angular.isUndefined(vm.modal)){
                vm.modal = $ionicModal.fromTemplate([
                    '<ion-modal-view>',
                    '    <ion-content class="has-tabs">',
                    '        <ry-resource slide="vm.slide"></ry-resource>',
                    '    </ion-content>',
                    '</ion-modal-view>'
                ].join(''), {
                    scope: $scope,
                    animation: 'slide-in-up'
                });
                vm.modal.scope.vm = vm;
                $scope.$on('$destroy', function() {
                    vm.modal.remove();
                });
            }
            vm.modal.show();
        }

        function iscurrentuser(pics){
            if(vm.selection.curr === 'favorites'){
                return (pics.resource_user_id != vm.me.id);
            }
            return (pics.user_id != vm.me.id);
        }
        function getcatfromid(cat_id){
            var tmp = $scope.findWhere(vm.categories, {id:cat_id});
            return tmp;
        }
        // get all pictures
        function getmyresources(off, user_id){
            var curr = select();
            var query = {username:username,
                token:token,
                user_id:user_id,
                offset:off||0,
                limit:100};
            query['categories[]'] = vm.catidarray;
            var newresources = curr.service.query(query,
                extractResources, endquery);
            return newresources.$promise;
        }
        function endquery(){
            var curr = select();
            curr.resourceMore = false;
            ryutils.toast('End of the list!');
        }
        function extractResources(data){
            var curr = select();
            if(data.length === 0){
                curr.resourceMore = false;
                ryutils.toast('End of the list!');
            }
            else{
                curr.resourceMore = true;
                curr.resources = curr.resources.concat(data);
            }
        }
        function getmoreresources(){
            var curr = select();
            var length = curr.resources.length;
            if(length < 0){return;}
            return getmyresources(length, vm.user.id).finally(function(){
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        function viewResources(pics) {
            shdata.current_resource_watched = pics;
            $state.go("tabs.resource");
        }
        function select(){
            return vm.selection[vm.selection.curr];
        }
        function resetpictures(tag){
            vm.selection[tag].resources = [];
            vm.selection[tag].resourceMore = true;
            $scope.each(vm.categories, initcat);
        }
        function initcat(cat){
            cat.deleted = false;
        }
    }

})();

(function(){
    angular.module('ry.menu.controllers', [
        'ionic', 'ratemy.services', 'ngMessages',
        'ngAnimate', 'ui.router', 'ionic-material',
        'ngCordova', 'angular-underscore'])
    .controller("menuCtrl", menuCtrl);
    menuCtrl.$inject = ["$scope", "$state", "$log",
        "ionicMaterialInk", "ionicMaterialMotion",
    "shdata", "config", "categories", "$timeout", "ryutils"];
    function menuCtrl($scope, $state, $log, ionicMaterialInk,
        ionicMaterialMotion, shdata, config, categories, $timeout, ryutils){
        var vm = this;
        vm.config = config;
        vm.categories = categories;
        vm.getcatstyle = getcatstyle;

        ryutils.showHeader();
        ryutils.clearFabs();
        ryutils.setExpanded(true);
        ryutils.setHeaderFab(false);
        ionicMaterialInk.displayEffect();
        $timeout(function() {
            ionicMaterialMotion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 200);
        function getcatstyle(cat){
            var style = {
                'background-image':'url('+ vm.config.domain + cat.image+')',
                'background-size':'cover',
                'background-position': 'center center'
            };
            return style;
        }
    }
})();

(function(){

    angular
        .module('ry.other.controllers',
                ['ionic', 'ratemy.services',
                 'ngMessages', 'ngAnimate', 'ui.router',
                 'ngCordova', 'angular-underscore'])
        .controller("otherCtrl", otherCtrl);
    otherCtrl.$inject = ["$scope", "$state", "shdata",
                         "config", "services_ratemy"];

    function otherCtrl($scope, $state, shdata, config,
                        services_ratemy){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.filterpopup = false;
        $scope.config = config;
        $scope.categories = shdata.get('categories');
        vm.me = shdata.me;
        vm.rankDetail = false;
        vm.filterlist = [{name: "score"},{name: "nb_views"},{name: "nb_shares"},{name: "nb_favorites"},{name: "nb_comments"},{name: "nb_resources"}];

        var tabSort = [{name: "score", css: "icon ion-wand"},{name: "nb_views", css: "icon ion-eye"},{name: "nb_shares", css: "icon ion-share"},{name: "nb_resources", css: "icon ion-images"},{name: "nb_comments", css: "icon ion-chatbubbles"},{name: "nb_favorites", css: "icon ion-heart"},{name: "nb_votes", css: "icon ion-star"}];

        vm.filterSelected = 'score';
        $scope.$watch('vm.filterSelected',function(sort){
            vm.sort = sort;
            vm.players = services_ratemy.users().query({
                username:username,
                token:token,
                limit:10,
                sort:sort});
            
            // PROBLEME ICI : il affiche la liste , mais la taille est de 0 ???
            console.log(vm.players);
            console.log(vm.players.length);
            
            // playerListe = permettra d'afficher le nom et les point dans le filtre actif 
            vm.playerListe = $scope.map(vm.players, function(player){
                console.log(player.username);
                var value = {'username':player.username,'point':player.sort};
                return value;
            });
                            console.log(vm.playerListe);

            
            for(var i = 0; i < tabSort.length; i++){
                if(tabSort[i].name == vm.sort)
                {
                    vm.logo = tabSort[i].css;   
                }
            }
        });




    }

})();

(function(){
 angular.module('ry.personaldetails.controllers',
     ['ionic', 'ratemy.services', 'ngMessages', 'ngAnimate', 'ui.router',
     'ngCordova', 'angular-underscore'])
    .controller("personalDetailsCtrl", personalDetailsCtrl);
      personalDetailsCtrl.$inject = ["$scope", "$state", "shdata",
      "services_ratemy", "$cordovaCapture", "ryutils",
      "$cordovaImagePicker", "config"];

    function personalDetailsCtrl($scope, $state, shdata, services_ratemy,
        $cordovaCapture, ryutils, $cordovaImagePicker, config){
    var vm = this;
    var username = shdata.get('username');
    var token = shdata.token;
    vm.config = config;
    vm.upload = upload;
    vm.me = shdata.me;
    vm.pass1 = shdata.get('password');
    vm.pass2 = vm.pass1;
    vm.myemail = vm.me.mail || "";
    vm.captureImage = captureImage;
    vm.selectImage = selectImage;
    function captureImage() {
        var options = { limit: 1 };
        $cordovaCapture.captureImage(options).then(function(imgs) {
            var img = imgs[0];
            vm.newpicture = img.fullPath;
        });
    }
    function selectImage() {
        var options = {
            maximumImagesCount:1,
            width: 0,
            height:0
        };
        $cordovaImagePicker.getPictures(options).then(function(imgs) {
            vm.newpicture = imgs[0];
        });
    }
    function upload() {
        var query = {
            'username': username,
            'token': token,
            'password': services_ratemy.hash(vm.pass1),
            'mail': vm.myemail
        };
        services_ratemy.change_details(query, vm.newpicture, uploadsucceed,
            uploaderr, uploadprog);
    }
    function uploadsucceed(){
        ryutils.toast('Changed details successfully');
        vm.uploading = false;
        vm.prog = '';
    }
    function uploaderr(err){
        ryutils.toast('Failed to change personal details!');
        vm.uploading = false;
        vm.prog = '';
        console.log(JSON.stringify(err));
    }
    function uploadprog(progress){
        vm.uploading = true;
        vm.prog = Math.floor(progress.loaded / progress.total * 100) + "%";
    }
}


})();

(function(){
    angular.module('ry.directives')
        .directive('ryReport', ryReport);

    ryReportsCtrl.$inject = ['$scope', '$element', '$attrs',
                             'shdata', 'config', 'services_ratemy', "$ionicPopup", "$timeout"];
    function ryReportsCtrl(scope, element, $attrs, shdata, config,
                            services_ratemy, $ionicPopup, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.showPopupReport = showPopupReport;
        vm.myreport = {};
        vm.myreport.css = "assertive";
        vm.reportReason = [{id: 1, name: "Insult"},{id: 2, name: "Explicit content"},{id: 3, name: "Offensive content"}];
        vm.reason = [];

        scope.$watch('vm.reason',function(reportReason){
            for(var i =0; i < vm.reportReason.length;i++)
            {
                if(vm.reportReason[i].name == reportReason)
                {
                    vm.idReason = vm.reportReason[i].id;
                }
            }
        });

        function report(){
            console.log("commentaire : " + vm.commentaire);
            console.log("id raison : " + vm.idReason);
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                comments:vm.commentaire,
                reason:vm.idReason
            }
            services_ratemy.reports().query(query);
        }

        /** init **/
        scope.$watch("vm.slide", function(newval, old){
            if(newval != old){
                activate();
            }
        });
        element.on("click", function(){
            vm.showPopupReport();
            scope.$apply();
        });
        /*
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };

            vm.myreport = services_ratemy.reports().get(query);
        } */
        function showPopupReport(){
            var reportPopup = $ionicPopup.confirm({
                title: 'Report',
                templateUrl: 'html/add_report_resource.html',
                scope:scope,
                buttons:[{ text: 'Cancel',
                          onTap: function(e) {
                          }
                         },
                         {
                             text: '<b>Submit</b>',
                             type: 'button-positive',
                             onTap: function(e) {
                                 // scope.reportMessage.$save();
                                 console.log("Report submitted");
                                 report();
                             }
                         },
                        ]
            });
        };
    }
    ryReport.$inject = ['services_ratemy', 'shdata', '$ionicPopup', 'config'];
    function ryReport(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button button-icon icon icon-left ion-alert"',
                'ng-class="vm.myreport.css"',
                'ng-click="vm.showPopupReport()">',
                '</a>'
            ].join(''),*/
            restrict: 'EA',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myreport: '='
            },
            controller:ryReportsCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();

(function(){
    angular
        .module('ry.directives')
        .directive("ryResource", ryResource);
    resourceCtrl.$inject = ["$scope", "$state", "shdata",
        "services_ratemy", "config",
        "ryutils", "$timeout", "ionicMaterialMotion", "ionicMaterialInk"];
    ryResource.$inject = ["services_ratemy", "shdata", "config"];
    function ryResource(services_ratemy, shdata, config) {
        var directive = {
            templateUrl: 'html/resource.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                slide: '='
            },
            controller:resourceCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {
            }
        };
        return directive;
    }
    function resourceCtrl(scope, $state, shdata,
                          services_ratemy, config, ryutils, $timeout,
                         ionicMaterialMotion, ionicMaterialInk){
       var vm = this;
       var username = shdata.username;
       var token = shdata.token;
       var me = shdata.me;
       vm.config = config;
       vm.showdescr = false;
       vm.getpict = ryutils.getpict;
       vm.pict = {
           'background-image': "url('" + vm.getpict(vm.slide.picture) +"')"
       };
       vm.getslidestyle = getslidestyle;

       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       /******/
       function activate(){
           var query = {
               username : username,
               token : token,
               user_id : me.id,
               resource_id : vm.slide.id,
               arrayexpected:false
           };
           vm.view = services_ratemy.resources_view().get(query,_postsave);
           // Ajout d'une vue
           scope.$on('ryCommentsLoaded', function(){
               scope.$emit('ryResourceHasChanged');
           });
           ionicMaterialInk.displayEffect();
       }
       function getslidestyle(slide){
           var style = {
               height: "300px",
               'background-size': 'cover',
               'background-position': 'center center'
               //'background-image': "url('" + vm.config.domain + slide.path + "')"
           };
           return style;
       }
       function _postsave(){
           vm.view.username = username;
           vm.view.user_id = me.id;
           vm.view.token = token;
           vm.view.resource_id = vm.slide.id;
           vm.view.$save();
       }
    }
})();

(function(){
    angular
    .module('ratemy.services', ['ratemy.config', 'ngCordova', 'ngAnimate',
    'ngResource'])
    .factory('services_ratemy', services_ratemy)
    .factory('service_logout', service_logout)
    .factory('service_checkusername', service_checkusername)
    .factory('ismobile', ismobile)
    .factory('service_islogged', service_islogged)
    .factory('config', configfunc)
    .factory('ryutils', ryutils)
    .factory('shdata', shdata);
   service_checkusername.$inject = ["$http", "config"];
   ryutils.$inject = ["$cordovaToast", "$window", "$log"];
   function service_checkusername($http, config){
        return function(username){
            return $http.post(config.domain + '/test/is_username_available',
                {'username' : username});
        };
    }
    function ismobile(){
        return document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
    }
    function service_islogged(shdata) {
        return function(){
            if(shdata.get('username') && shdata.get('password')){
                return true;
            }
            return false;
        };
    }
    function shdata($window) {
        var ramdata = {
            get: getfunc,
            set: setfunc,
        };
        return ramdata;
        function getfunc(key, dft){
            var obj = JSON.parse($window.localStorage.ratemydata || '{}');
            if(key in obj){
                ramdata[key] = obj[key];
            }
            if(key in ramdata){
                return ramdata[key];
            }
            return dft;
        }
        function setfunc(key, value){
            var obj = JSON.parse($window.localStorage.ratemydata || '{}');
            obj[key] = value;
            $window.localStorage.ratemydata = JSON.stringify(obj);
            ramdata[key] = value;
            return ramdata[key];
        }
    }

    function service_logout($http, $ionicLoading, config){
        return function(username, token, success, error){
            $ionicLoading.show({
                template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner>'
            });
            $http.post(config.domain + '/logout',
                {'username' : username, 'token': token})
                .success(success).error(error).finally(function(){
                    $ionicLoading.hide();
                });

        };

    }
    function services_ratemy($http, $ionicLoading, config,
        $cordovaFileTransfer, $resource, $cordovaDevice, ismobile,
        $document)
    {
        return {
            'hiscores': users,
            'hash': hash,
            'users': users,
            'reports': reports,
            'config': configreq,
            'categories': categories,
            'comments': comments,
            'resources': resources,
            'favorites': favorites,
            'shares': shares,
            'comments_vote': comments_vote,
            'resources_vote': resources_vote,
            'resources_view': resources_view,
            'change_details': change_details,
            'images' : {'get': get_resources,
                'insert': insert_resources},
        };
    function arg_dft(arg, val){
        return (typeof(arg) !== 'undefined' && arg !== null) ? arg : val;
    }
    function get_resources(username, token, resource_id, user, categories,
        offset, limit, success, error)
        {
            offset = arg_dft(offset, 0);
            limit = arg_dft(limit, 1);
            $http.get(config.domain + '/api/v1.0/resources',
                {'username' : username, 'token': token,
                    'categories': categories, 'user':user,
                    'resource_id':resource_id, 'offset': offset,
                    'limit': limit})
                    .success(success).error(error);
        }
        function insert_resources(params,
            filepath, success, error, progress){
                var options = {'params':params};
                document.addEventListener('deviceready', function () {

                    $cordovaFileTransfer.upload(config.domain + '/api/v1.0/resources',
                        filepath, options, true)
                        .then(success, error , progress);
                }, false);
            }
            function change_details(details, filepath, success, error, progress){
                var options = {'params':details};
                document.addEventListener('deviceready', function () {
                    if(filepath){
                        $cordovaFileTransfer.upload(config.domain + '/api/v1.0/users',
                            filepath, options, true)
                            .then(success, error , progress);
                    }else{
                        $http.post(config.domain + '/api/v1.0/users',
                            details).success(success).error(error);
                    }
                }, false);
            }
            function getuuid(){
                if(ismobile){
                    return $cordovaDevice.getUUID();
                }
                return "not a mobile";
            }
            function users(){
                return $resource(config.domain + '/api/v1.0/users/:id', {id:'@id'},
                    {
                        login: {
                            method:"GET",
                            params:{
                                login:true,
                                uuid:getuuid()
                            }
                        },
                        register: {
                            method:"PUT",
                            params:{
                                register:true,
                                uuid:getuuid()
                            }
                        }
                    }
                );
            }
            function comments(){
                return $resource(config.domain + '/api/v1.0/comments', {}, {});
            }
            function reports(){
                return $resource(config.domain + '/api/v1.0/reports', {}, {});
            }
            function comments_vote(){
                return $resource(config.domain + '/api/v1.0/comments/votes', {}, {});
            }
            function resources(){
                return $resource(config.domain + '/api/v1.0/resources', {}, {});
            }
            function categories(){
                return $resource(config.domain + '/api/v1.0/categories', {}, {});
            }
            function resources_vote(){
                return $resource(config.domain + '/api/v1.0/resources/vote', {}, {});
            }
            function resources_view(){
                return $resource(config.domain + '/api/v1.0/resources/view', {}, {});
            }
            function favorites(){
                return $resource(config.domain + '/api/v1.0/favorites', {}, {});
            }
            function shares(){
                return $resource(config.domain + '/api/v1.0/shares', {}, {});
            }
            function configreq(){
                return $resource(config.domain + '/api/v1.0/config', {}, {});
            }

            function hash(data){
                var sha_obj = new jsSHA(data, "TEXT");
                var hashdata = sha_obj.getHash("SHA-512", "HEX");
                return hashdata;
            }
    }
    function ryutils($cordovaToast, $window, $log){
        var _priv = {};
        var data = {
            toast: toast,
            getpict: getpict,
            err: err,
            hideNavBar: hideNavBar,
            showNavBar: showNavBar,
            noHeader: noHeader,
            setExpanded: setExpanded,
            setHeaderFab: setHeaderFab,
            isExpanded: isExpanded,
            isHeaderFabLeft: isHeaderFabLeft,
            isHeaderFabRight: isHeaderFabRight,
            hasHeader: hasHeader,
            hideHeader: hideHeader,
            showHeader: showHeader,
            clearFabs: clearFabs
        };
        return data;
        function err(resp){
            $log.log(resp);
        }
        function hideNavBar() {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        }
        function showNavBar() {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        }
        function noHeader() {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }
        }
        function setExpanded(bool) {
            _priv.isExpanded = bool;
        }
        function isExpanded() {
            return _priv.isExpanded;
        }
        function isHeaderFabLeft() {
            return _priv.hasHeaderFabLeft;
        }
        function isHeaderFabRight() {
            return _priv.hasHeaderFabRight;
        }
        function setHeaderFab(location) {
            var hasHeaderFabLeft = false;
            var hasHeaderFabRight = false;

            switch (location) {
                case 'left':
                    hasHeaderFabLeft = true;
                    break;
                case 'right':
                    hasHeaderFabRight = true;
                    break;
            }

            _priv.hasHeaderFabLeft = hasHeaderFabLeft;
            _priv.hasHeaderFabRight = hasHeaderFabRight;
        }

        function hasHeader() {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (!content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }

        }

        function hideHeader() {
            hideNavBar();
            noHeader();
        }

        function showHeader() {
            showNavBar();
            hasHeader();
        }

        function clearFabs() {
            var fabs = document.getElementsByClassName('button-fab');
            if (fabs.length && fabs.length > 1) {
                fabs[0].remove();
            }
        }
        function toast(msg){
            if(typeof($window.plugins) === 'undefined'){
                $log.log("Toast : " + msg);
            }else{
                $cordovaToast.showShortBottom(msg);
            }
        }
        function getpict(pict){
            var _pict = pict;
            if(_pict === null || typeof _pict === "undefined"){
                _pict = configfunc().default_picture;
            }
            return configfunc().domain + _pict;
        }
    }
    function configfunc(){
        var data = {
            //'domain': 'http://192.168.1.21/ratemy',
            domain: 'http://ratemy.changala.fr',
            default_picture: "/profiles/default_profile.png",
            getcatclasstype: function(type){
                var types = ['ion-camera', 'ion-volume-high', 'ion-videocamera',
                    'ion-document-text'];
                if(type >= 0 && type < types.length){
                    return types[type];
                }
                return types[3];
            }
        };

        return data;
    }
})();

(function(){
    angular.module('ry.directives')
    .directive('ryShare', ryShare);

    ryShareCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', "$timeout"];
    function ryShareCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.addOrDeleteShares = addOrDeleteShares;

        /** init **/
       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       element.on("click", function(){
           vm.addOrDeleteShares();
           scope.$apply();
       });

        function addOrDeleteShares(){
            if(angular.isUndefined(vm.myshare.id) || vm.myshare.id == 0){
                vm.myshare.resource_id = vm.slide.id;
                vm.myshare.username = username;
                vm.myshare.token = token;
                vm.myshare.$save();
            }else{
                vm.myshare.$delete({username:username,
                    token:token,
                    resource_id:vm.slide.id},function(data){
                        vm.myshare = data;
                    });
            }
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myshare = services_ratemy.shares().get(query);
            scope.$watch('vm.myshare.id', function(newval){
                if(angular.isUndefined(newval) || newval == 0){
                    vm.myshare.css = "dark";
                }else{
                    vm.myshare.css = "positive";
                }

            });
        }
    }
    ryShare.$inject = ['services_ratemy', 'shdata', '$ionicPopup',
    'config'];
    function ryShare(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button icon button-icon icon-left ion-share"',
                'ng-class="vm.myshare.css"',
                'ng-click="vm.share()">',
                '</a>'
            ].join(''),*/
            restrict: 'AE',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myshare: '='
            },
            controller:ryShareCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();

(function(){
    angular.module('ry.directives')
    .directive('stackHeaders', stackHeaders)
    .directive('stackHeader', stackHeader);
    stackHeader.$inject = [];
    stackHeaders.$inject = [];
    function stackHeader() {
        var index = 0;
        return {
            require: '^^stackHeaders',
            restrict: 'A',
            controller:stackHeaderCtrl,
            link: function(scope, el, attrs, ctrl) {
                ctrl.headers.push(el);
                index = ctrl.headers.length - 1;
                el.css('transition', 'transform .5s ease-in-out');

                scope.$on('$destroy', function() {
                    ctrl.headers.splice(index, 1);
                });
            },
        };
    }
    stackHeaderCtrl.$inject = ["$scope", "$element", "$attrs", "$timeout", "$interval", "$ionicPosition", "$ionicScrollDelegate"];
    function stackHeaderCtrl(scope, el, at, $timeout, $interval, $ionicPosition, $ionicScrollDelegate){
        var vm = this;
    }
    
    
    stackHeadersCtrl.$inject = ["$scope", "$element", "$attrs", "$timeout", "$interval", "$ionicPosition", "$ionicScrollDelegate"];
    function stackHeadersCtrl(scope, el, at, $timeout, $interval, $ionicPosition, $ionicScrollDelegate){
        var vm = this;
        vm.headers = [];
        at.$set('scroll-event-interval', 5);


        el.on('scroll', function(e) {
            for(var i=0; i < vm.headers.length; i++){
                var header = vm.headers[i][0];
                var headerHeight = header.offsetHeight;
                var offsetTop = header.offsetTop;
                var scrollTop = null;
                var header2 = null;
                var offsetTop2 = 0;
                var inter2 = 0;
                if(e.detail){
                    scrollTop = e.detail.scrollTop;
                }else if(e.target){
                    scrollTop = e.target.scrollTop;
                }
                if(i < vm.headers.length - 1){
                    header2 = vm.headers[i+1][0];
                    offsetTop2 = header2.offsetTop;
                    inter2 = Math.max(0,scrollTop - offsetTop2 + headerHeight);
                }
                else{
                    inter2 = 0;
                }
                var inter = scrollTop - offsetTop - inter2;
                var dist = Math.max(0, inter);
                shrink(header, scrollTop - offsetTop, dist, headerHeight);
            }
        });
        function shrink(header, scrollTop, amt, max) {
            var fadeAmt = (max - Math.min(Math.max((scrollTop - amt), 0), max))/max;
            ionic.requestAnimationFrame(function() {
                header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + amt + 'px, 0)';
                for(var i = 0, j = header.children.length; i < j; i++) {
                    header.children[i].style.opacity = fadeAmt;
                }
            });
        }

    }

    function stackHeaders(){

        return {
            restrict: 'A',
            controller:stackHeadersCtrl,
            link: function (scope, el, attrs, ctrl) {
            }
        };

    }

})();

(function(){
    angular.module('ry.directives')
    .directive('ryStats', ryStats);

    ryStats.$inject = ["$timeout"];
    function ryStats($timeout) {
        return {
            templateUrl: 'html/ry-stats.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                ryUser: '='
            },
            controller:['$scope', '$element', '$attrs',
                function($scope, $element, $attrs) {
            }],
            link: function (scope, element, attrs, ctrl) {
                var _def = ["",
                    "number of comments",
                    "number of favorite",
                    "number of report",
                    "number of resource",
                    "number of share",
                    "number of view",
                    "number of vote",
                "number of score"];
            var mytimeout = null;
            scope.showDef = function(definition){
                scope.show = true;
                if(mytimeout !== null){
                    $timeout.cancel(mytimeout);
                    mytimeout = null;
                }
                mytimeout = $timeout(function() {
                    scope.show = false;
                    mytimeout = null;
                }, 3500);
                scope.definition = _def[definition];
            };
            }
        };
    }

})();

(function(){
    angular.module('ry.directives')
    .directive('samepassword', samepassword)
    .directive('alreadytaken',  alreadytaken)
    .directive('ngInfo', ngInfo)
    .factory('ClosePopupService', ClosePopupService);

    alreadytaken.$inject = ["$q", "service_checkusername"];
    function samepassword() {
        return {
            require: 'ngModel',
            scope:{
                samepassword:"="
            },
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.samepassword = function(modelValue, viewValue) {
                    if(modelValue === ""){
                        return false;
                    }
                    return (scope.samepassword == modelValue);
                };
            }
        };
    }

    function alreadytaken($q, service_checkusername) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$asyncValidators.alreadytaken = function(modelValue, viewValue) {
                    if(scope.iscreation){
                        return service_checkusername(modelValue);
                    }else{
                        /* if it is login, do not check username */
                        var def = $q.defer();
                        def.resolve();
                        return def.promise;
                    }
                };
            }
        };
    }
    function ngInfo() {
        return {
            link : function(scope, element, attrs) {
                var img = angular.element('<i class="icon icon-left calm ion-information-circled placeholder-icon"></i>');
                element.prepend(img);
            },
        };
    }
    ClosePopupService.$inject = ['$document', '$ionicPopup', '$timeout'];
    function ClosePopupService($document, $ionicPopup, $timeout){
        var lastPopup;
        var popupservice = {};
        popupservice.register = register;
        return popupservice;
        function register(popup) {
            $timeout(function(){
                var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                if(!element || !popup || !popup.close) return;
                element = element && element.children ? angular.element(element.children()[0]) : null;
                lastPopup = popup;
                var insideClickHandler = function(event){
                    event.stopPropagation();
                };
                var outsideHandler = function() {
                    popup.close();
                };
                element.on('click', insideClickHandler);
                $document.on('click', outsideHandler);
                popup.then(function(){
                    lastPopup = null;
                    element.off('click', insideClickHandler);
                    $document.off('click', outsideHandler);
                });
            });
        }
    }

})();

(function(){
    angular.module('ry.directives')
    .directive('ryVote', ryVote);

    ryVoteCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', '$ionicPopup', '$timeout',
    'ClosePopupService', '$timeout'];
    function ryVoteCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $ionicPopup, $timeout, ClosePopupService,
    $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        var popupdetails = {
            templateUrl: 'html/ry-vote.html',
            title: 'Rate it!',
            subTitle: '',
            scope: scope,
            buttons: [ ]
        };
        vm.config = config;
        vm.me = shdata.me;
        vm.prevnote = 0;
        vm.stars = [1, 2, 3, 4, 5];
        vm.votedclass = [
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline"
        ];
        vm.vote = vote;
        vm.openVote = openVote;

        /** init **/
        activate();
        element.on("click", function(){
            openVote();
            scope.$apply();
        });

        function openVote(){
            vm.votepopup = $ionicPopup.show(popupdetails);
            ClosePopupService.register(vm.votepopup);
        }
        function updateStars($index){
            var itr = 0;
            for (itr=0; itr < vm.stars.length; itr++) {
                if(itr <= $index){
                    vm.votedclass[itr] = "ion-android-star";
                }
                else{
                    vm.votedclass[itr] = "ion-android-star-outline";
                }
            }
        }
        function vote($index) {
            if(vm.prevnote != $index){
                vm.myvote.username = username;
                vm.myvote.token = token;
                vm.myvote.resource_id = vm.slide.id;
                vm.myvote.note = $index + 1;
                vm.myvote.$save(function(){
                    vm.prevnote = $index;
                });
                updateStars($index);
                updateNote(vm.myvote.note);
            }
            $timeout(function(){
                vm.votepopup.close();
            }, 500);
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myvote = services_ratemy.resources_vote().get(query, function(data){
                updateNote(data.note);
                vm.myvote.username = username;
                vm.prevnote = data.note - 1;
                updateStars(vm.prevnote);
            });
        }
        function updateNote(note){
            if(typeof note === "undefined"){
                vm.myvote.css = "black";
                vm.prevnote = 0;
            }else{
                vm.myvote.css = "royal";
                vm.prevnote = note - 1;
            }
        }
    }
    ryVote.$inject = ['services_ratemy', 'shdata', '$ionicPopup', 'config'];
    function ryVote(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            restrict: 'A',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myvote: '='
            },
            controller:ryVoteCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();
