(function(){
    angular.module('ry.directives')
    .directive('ryFavorite', ryFavorite);

    ryFavoriteCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', "$timeout"];
    function ryFavoriteCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.addOrDeleteFavorites = addOrDeleteFavorites;

        /** init **/
       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       element.on("click", function(){
           vm.addOrDeleteFavorites();
           scope.$apply();
       });

        function addOrDeleteFavorites(){
            if(angular.isUndefined(vm.myfavorite.id) || vm.myfavorite.id == 0){
                vm.myfavorite.resource_id = vm.slide.id;
                vm.myfavorite.username = username;
                vm.myfavorite.token = token;
                vm.myfavorite.$save();
            }else{
                vm.myfavorite.$delete({username:username,
                    token:token,
                    resource_id:vm.slide.id},function(data){
                        vm.myfavorite = data;
                    });
            }
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myfavorite = services_ratemy.favorites().get(query);
            scope.$watch('vm.myfavorite.id', function(newval){
                if(angular.isUndefined(newval) || newval == 0){
                    vm.myfavorite.css = "dark";
                }else{
                    vm.myfavorite.css = "balanced";
                }

            });
        }
    }
    ryFavorite.$inject = ['services_ratemy', 'shdata', '$ionicPopup',
    'config'];
    function ryFavorite(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button icon button-icon icon-left ion-heart"',
                'ng-class="vm.myfavorite.css"',
                'ng-click="vm.addOrDeleteFavorites()">',
                '</a>'
            ].join(''),*/
            restrict: 'AE',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myfavorite: '='
            },
            controller:ryFavoriteCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();
