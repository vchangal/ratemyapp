(function(){
 angular.module('ry.personaldetails.controllers',
     ['ionic', 'ratemy.services', 'ngMessages', 'ngAnimate', 'ui.router',
     'ngCordova', 'angular-underscore'])
    .controller("personalDetailsCtrl", personalDetailsCtrl);
      personalDetailsCtrl.$inject = ["$scope", "$state", "shdata",
      "services_ratemy", "$cordovaCapture", "ryutils",
      "$cordovaImagePicker", "config"];

    function personalDetailsCtrl($scope, $state, shdata, services_ratemy,
        $cordovaCapture, ryutils, $cordovaImagePicker, config){
    var vm = this;
    var username = shdata.get('username');
    var token = shdata.token;
    vm.config = config;
    vm.upload = upload;
    vm.me = shdata.me;
    vm.pass1 = shdata.get('password');
    vm.pass2 = vm.pass1;
    vm.myemail = vm.me.mail || "";
    vm.captureImage = captureImage;
    vm.selectImage = selectImage;
    function captureImage() {
        var options = { limit: 1 };
        $cordovaCapture.captureImage(options).then(function(imgs) {
            var img = imgs[0];
            vm.newpicture = img.fullPath;
        });
    }
    function selectImage() {
        var options = {
            maximumImagesCount:1,
            width: 0,
            height:0
        };
        $cordovaImagePicker.getPictures(options).then(function(imgs) {
            vm.newpicture = imgs[0];
        });
    }
    function upload() {
        var query = {
            'username': username,
            'token': token,
            'password': services_ratemy.hash(vm.pass1),
            'mail': vm.myemail
        };
        services_ratemy.change_details(query, vm.newpicture, uploadsucceed,
            uploaderr, uploadprog);
    }
    function uploadsucceed(){
        ryutils.toast('Changed details successfully');
        vm.uploading = false;
        vm.prog = '';
    }
    function uploaderr(err){
        ryutils.toast('Failed to change personal details!');
        vm.uploading = false;
        vm.prog = '';
        console.log(JSON.stringify(err));
    }
    function uploadprog(progress){
        vm.uploading = true;
        vm.prog = Math.floor(progress.loaded / progress.total * 100) + "%";
    }
}


})();
