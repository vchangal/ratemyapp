(function(){
        angular
        .module('ry.me.controllers',
            ['ionic', 'ratemy.services', 'ngMessages', 'ngAnimate',
                'ui.router', 'ngCordova', 'angular-underscore'])
        .controller("meCtrl", meCtrl);
    meCtrl.$inject = ["$scope", "$state", "shdata", "services_ratemy",
    "config", "$ionicPopup", "$ionicModal", "$stateParams", "ryutils",
    "$timeout", "ionicMaterialInk", "ionicMaterialMotion"];
    function meCtrl($scope, $state, shdata, services_ratemy,
                     config, $ionicPopup, $ionicModal,$stateParams,
                     ryutils, $timeout, ionicMaterialInk, ionicMaterialMotion){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.get('token');
        vm.config = config;
        vm.me = shdata.me;
        vm.categories = shdata.categories;
        vm.viewResources = viewResources;
        vm.getmoreresources = getmoreresources;
        vm.select = select;
        vm.selection = {curr:"pictures"};
        vm.selection.pictures = {
            service:services_ratemy.resources(),
            title:"User's Pictures:",
            csstitle:"ion-images"
        };
        vm.selection.favorites = {
            service:services_ratemy.favorites(),
            title:"User's Favorites:",
            csstitle:"ion-heart"
        };
        vm.resetpictures = resetpictures;
        vm.getcatfromid = getcatfromid;
        vm.iscurrentuser = iscurrentuser;
        vm.showResource = showResource;
        vm.getpict = ryutils.getpict;

        /**init ctrl*/
        activate();
        // self information
        function getuser(user_id){
            var query = {username:username,
                token:token,
                id:user_id,
                offset:0,
                limit:1
            };
            var user = services_ratemy.users().get(query);
            return user;
        }
        function activate(){
            var id = $stateParams.user_id;
            ryutils.showHeader();
            ryutils.clearFabs();
            ryutils.setExpanded(true);
            ryutils.setHeaderFab(false);
            ionicMaterialInk.displayEffect();
            $timeout(function() {
                ionicMaterialMotion.slideUp({
                    selector: '.slide-up'
                });
            }, 300);

            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 3000
                });
            }, 700);

            resetpictures("favorites");
            resetpictures("pictures");
            if(id == 0){
                vm.user = shdata.me;
                vm.user.user_id = vm.user.id;
            }
            else{
                vm.user = getuser(id);
            }
            var tmp = $scope.filter(vm.categories,
                                    function(cat){return !cat.deleted;});
            vm.catidarray = $scope.map(tmp, function(cat){return cat.id;});
            vm.pict = {
                'background-image': "url('" + vm.getpict(vm.user.picture || vm.config.default_picture) +"')"
        };
        }

        function showResource(pic){
            vm.slide = pic;
            if(angular.isUndefined(vm.modal)){
                vm.modal = $ionicModal.fromTemplate([
                    '<ion-modal-view>',
                    '    <ion-content class="has-tabs">',
                    '        <ry-resource slide="vm.slide"></ry-resource>',
                    '    </ion-content>',
                    '</ion-modal-view>'
                ].join(''), {
                    scope: $scope,
                    animation: 'slide-in-up'
                });
                vm.modal.scope.vm = vm;
                $scope.$on('$destroy', function() {
                    vm.modal.remove();
                });
            }
            vm.modal.show();
        }

        function iscurrentuser(pics){
            if(vm.selection.curr === 'favorites'){
                return (pics.resource_user_id != vm.me.id);
            }
            return (pics.user_id != vm.me.id);
        }
        function getcatfromid(cat_id){
            var tmp = $scope.findWhere(vm.categories, {id:cat_id});
            return tmp;
        }
        // get all pictures
        function getmyresources(off, user_id){
            var curr = select();
            var query = {username:username,
                token:token,
                user_id:user_id,
                offset:off||0,
                limit:100};
            query['categories[]'] = vm.catidarray;
            var newresources = curr.service.query(query,
                extractResources, endquery);
            return newresources.$promise;
        }
        function endquery(){
            var curr = select();
            curr.resourceMore = false;
            ryutils.toast('End of the list!');
        }
        function extractResources(data){
            var curr = select();
            if(data.length === 0){
                curr.resourceMore = false;
                ryutils.toast('End of the list!');
            }
            else{
                curr.resourceMore = true;
                curr.resources = curr.resources.concat(data);
            }
        }
        function getmoreresources(){
            var curr = select();
            var length = curr.resources.length;
            if(length < 0){return;}
            return getmyresources(length, vm.user.id).finally(function(){
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        function viewResources(pics) {
            shdata.current_resource_watched = pics;
            $state.go("tabs.resource");
        }
        function select(){
            return vm.selection[vm.selection.curr];
        }
        function resetpictures(tag){
            vm.selection[tag].resources = [];
            vm.selection[tag].resourceMore = true;
            $scope.each(vm.categories, initcat);
        }
        function initcat(cat){
            cat.deleted = false;
        }
    }

})();
