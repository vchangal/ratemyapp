(function(){
    angular.module('ry.directives')
    .directive('ryInsertResource', ryInsertResource);

    ryInsertResource.$inject = ['$cordovaFile',
        '$cordovaCapture', '$ionicPlatform', '$ionicModal', 'shdata',
        '$cordovaToast', 'config', 'services_ratemy', '$parse'];
    ryInsertResourceCtrl.$inject = ["$scope", "$element", "$attrs",
        '$cordovaFile', '$cordovaCapture', '$ionicPlatform', '$ionicModal',
        'shdata', '$cordovaToast', 'config', 'services_ratemy', '$parse',
        'ryutils', "$cordovaImagePicker"
    ];
    function ryInsertResourceCtrl(scope, element, attrs, $cordovaFile,
        $cordovaCapture, $ionicPlatform, $ionicModal, shdata, $cordovaToast,
        config, services_ratemy, $parse, ryutils, $cordovaImagePicker) {
            var vm = this;
            var fn = $parse(attrs['ng-click']);
            vm.uploading = false;
            vm.prog = "Upload";
            vm.categories = shdata.categories;
            vm.register_logo= [
                'ion-camera',
                'ion-mic-c',
                'ion-videocamera',
                'ion-compose'
            ];
            var username = shdata.get('username');
            var token = shdata.token;

            vm.upload = upload;
            vm.cat = vm.ryCat;
            vm.captureResource = captureResource;
            vm.selectResource = selectResource;
            vm.getimg = getimg;

            activate();

            function activate(){
                element.bind('click', function(event) {

                    //running the function
                    scope.$apply(function() {
                        fn(scope, {$event:event});
                        vm.modal.show();
                    });
                });
                $ionicPlatform.ready(function(){
                    $ionicModal.fromTemplateUrl('html/add_change_resource.html', {
                        scope: scope,
                        hardwareBackButtonClose: true,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        vm.modal = modal;
                    });
                });
                scope.$on('$destroy', function() {
                    vm.modal.remove();
                });
            }
            function selectImage() {
                var options = {
                    maximumImagesCount:1,
                    width: 0,
                    height:0
                };
                $cordovaImagePicker.getPictures(options).then(function(imgs) {
                    vm.img_path = imgs[0];
                });
            }
            function selectResource(){
                var capt = [
                    selectImage,
                    null,
                    null
                ];
                if(vm.ryType < 0 && vm.ryType > 2){
                    return selectImage;
                }
                capt[vm.ryType]();
            }
            function captureResource(){
                var capt = [
                    captureImage,
                    null,
                    null
                    /*scope.captureAudio,
                scope.captureVideo*/
                ];
                if(vm.ryType < 0 && vm.ryType > 2){
                    return captureImage;
                }
                capt[vm.ryType]();
            }
            function captureImage() {
                var options = { limit: 1 };
                $cordovaCapture.captureImage(options).then(function(imgs) {
                    var img = imgs[0];
                    vm.img_path = img.fullPath;
                });
            }
            function upload() {
                var params = {
                    username: username,
                    token: token,
                    name: vm.name,
                    description: vm.descr,
                    type: vm.ryType,
                    categories: vm.cat.id
                };
                vm.uploading = true;
                /*if(scope.ryType == 3){
                    vm.img_path = captureText(vm.img_path);
                }*/
                services_ratemy.images.insert(params,
                    vm.img_path, _success, _err, _prog);
            }
            function _success(data){
                ryutils.toast(vm.name + ' uploaded!');
                vm.uploading = false;
                vm.modal.hide();
            }
            function _err(err){
                ryutils.toast('Failed to upload!');
                vm.uploading = false;
                vm.prog = "Upload";
                console.log(JSON.stringify(err));
            }
            function _prog(progress){
                var _val = progress.loaded / progress.total * 100;
                vm.prog = Math.floor(_val) + "%";
            }

            function getimg(){
                var style = {
                    height: "300px",
                    'background-size': 'cover',
                    'background-position': 'center center',
                    'background-image': "url('" + vm.img_path + "')"
                };
                return style;
            }
        }
    function ryInsertResource($cordovaFile, $cordovaCapture,
        $ionicPlatform, $ionicModal, shdata, $cordovaToast, config,
        services_ratemy, $parse) {
            return {
                restrict: 'A',
                templateNamespace: 'html',
                scope: {
                    ryCat: '=',
                    ryType: '='
                },
                controller:ryInsertResourceCtrl,
                bindToController: true,
                controllerAs: "vm",
                link: function (scope, element, attrs, ctrl) {
                }
            };
        }
})();
