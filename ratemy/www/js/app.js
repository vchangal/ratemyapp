// Ionic Starter App

(function(){
    angular
        .module('ratemy', [
            'ionic',
            'ionic-material',
            'ry.global.controllers',
            'ry.personaldetails.controllers',
            'ry.menu.controllers',
            'ry.me.controllers',
            'ry.other.controllers',
            'ry.cat.controllers',
            'ry.login.controllers',
            'ratemy.services',
            'ngMessages',
            'ngAnimate',
            'ry.directives',
            'ratemy.directives'
        ])
        .run(runFunction)
        .config(configFunction);
    runFunction.$inject = ["$rootScope", "$ionicPlatform",
                           "$window"];
    configFunction.$inject = ["$stateProvider",
                              "$urlRouterProvider",
                              "$ionicConfigProvider",
                              "$sceDelegateProvider",
                              "$sceProvider",
                              "$compileProvider"];
    function runFunction($rootScope, $ionicPlatform, $window) {
        $ionicPlatform.ready(readyFunction);
        function readyFunction() {
            if($window.cordova && $window.cordova.plugins &&
               $window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if($window.StatusBar) {
                StatusBar.styleDefault();
            }
        }
    }
    function configFunction($stateProvider, $urlRouterProvider,$ionicConfigProvider,
                             $sceDelegateProvider, $sceProvider,
                             $compileProvider) {
        $sceProvider.enabled(true);
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://test.changala.fr/**',
            'file:**',
            'data:**'
        ]);
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);
        $ionicConfigProvider.tabs.position('bottom');
        $stateProvider
            .state('home', {
            url: '/home',
            templateUrl: 'html/home.html'
        })
            .state('login', {
            url: '/login',
            controller: 'loginCtrl',
            controllerAs: 'vm',
            templateUrl: 'html/login.html'
        })
            .state('tabs', {
            url: '/tabs',
            abstract: true,
            templateUrl: 'html/menu.html',
            resolve: {
                categories: function(services_ratemy, shdata, ryutils, $q){
                    var _categories = $q(function(resolve, reject){
                        resolve(shdata.categories || null);
                    });
                    var categories = _categories.then(function(cats){
                        if(cats === null){
                            return services_ratemy.categories().query({
                                username:shdata.get('username'),
                                token:shdata.get('token')
                            }, savecat, null).$promise;
                        }
                        return cats;
                    });
                    return categories;
                    function savecat(cats){
                        ryutils.toast('Login successful!');
                        shdata.set('categories', cats, false);
                        return cats;
                    }
                }
            },
        })
            .state('tabs.menu', {
            url: '/menu',

            views:{
                menuContent: {
                    controller: 'menuCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/menucat.html'
                }
            }
        })
            .state('tabs.self', {
            url: '/self/:user_id',
            params:{
                user_id:0,
            },
            views:{
                menuContent: {
                    controller: 'meCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/me.html'
                }
            }
        })
            .state('tabs.other', {
            url: '/other',
            views:{
                menuContent: {
                    controller: 'otherCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/other.html'
                }
            }
        })
            .state('tabs.meView', {
            url: '/other/:user_id',
            views:{
                menuContent: {
                    controller: 'meCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/me.html'
                }
            }
        })
            .state('tabs.cat', {
                url: '/cat/:type/:cat',
            params:{
                type:0
            },
            resolve: {
                slides: function(services_ratemy, shdata, ryutils, config,
                    $stateParams, $ImageCacheFactory, $rootScope){
                    var type = $stateParams.type;
                    var cat = $stateParams.cat || null;
                    var query = {};
                    query.username = shdata.username;
                    query.token = shdata.token;
                    if(cat !== null){
                        var catselected = shdata.categories[cat];
                        query.categories = catselected.id;
                    }
                    query.type = type;
                    query.offset = 0;
                    query.limit = 4;
                    var slides = services_ratemy.resources().query(query, saveslide);
                    return slides.$promise;
                    function saveslide(slides){
                        var imgs = $rootScope.map(slides, function(pic){return config.domain + pic.path;});
                        //return $ImageCacheFactory.Cache(imgs).then(function(){
                            return slides.$promise;
                        //});
                    }
                }
            },
            views:{
                menuContent: {
                    controller: 'catCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/cat.html'
                },
                fabContent: {
                    template: ['<button id="fab-gallery" class="button button-fab button-fab-top-right expanded button-energized-900 drop"',
                        'ry-type="type"',
                        'ry-cat="cat"',
                        'ry-insert-resource >',
                        '<i class="icon ion-camera"></i>',
                    '</button>'].join(''),
                    controller: function ($scope, $timeout, $stateParams, shdata) {
                        $scope.type = $stateParams.type;
                        if($stateParams.cat){
                            $scope.cat = shdata.categories[$stateParams.cat] || null;
                        }
                        $timeout(function () {
                            document.getElementById('fab-gallery').classList.toggle('on');
                        }, 600);
                    }
                }
            }
            })
            .state('tabs.resource', {
            url: '/resource',
            views:{
                menuContent: {
                    templateUrl: 'html/resourceView.html'
                }

            }
        })
        .state('tabs.details', {
            url: '/config',
            views:{
                menuContent: {
                    controller: 'personalDetailsCtrl',
                    controllerAs: 'vm',
                    templateUrl: 'html/personalDetails.html'
                }
            }
        });
        $urlRouterProvider.otherwise("/home");
    }
})();
