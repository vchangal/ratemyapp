(function(){

    angular
        .module('ry.other.controllers',
                ['ionic', 'ratemy.services',
                 'ngMessages', 'ngAnimate', 'ui.router',
                 'ngCordova', 'angular-underscore'])
        .controller("otherCtrl", otherCtrl);
    otherCtrl.$inject = ["$scope", "$state", "shdata",
                         "config", "services_ratemy"];

    function otherCtrl($scope, $state, shdata, config,
                        services_ratemy){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.filterpopup = false;
        $scope.config = config;
        $scope.categories = shdata.get('categories');
        vm.me = shdata.me;
        vm.rankDetail = false;
        vm.filterlist = [{name: "score"},{name: "nb_views"},{name: "nb_shares"},{name: "nb_favorites"},{name: "nb_comments"},{name: "nb_resources"}];

        var tabSort = [{name: "score", css: "icon ion-wand"},{name: "nb_views", css: "icon ion-eye"},{name: "nb_shares", css: "icon ion-share"},{name: "nb_resources", css: "icon ion-images"},{name: "nb_comments", css: "icon ion-chatbubbles"},{name: "nb_favorites", css: "icon ion-heart"},{name: "nb_votes", css: "icon ion-star"}];

        vm.filterSelected = 'score';
        $scope.$watch('vm.filterSelected',function(sort){
            vm.sort = sort;
            vm.players = services_ratemy.users().query({
                username:username,
                token:token,
                limit:10,
                sort:sort});
            
            // PROBLEME ICI : il affiche la liste , mais la taille est de 0 ???
            console.log(vm.players);
            console.log(vm.players.length);
            
            // playerListe = permettra d'afficher le nom et les point dans le filtre actif 
            vm.playerListe = $scope.map(vm.players, function(player){
                console.log(player.username);
                var value = {'username':player.username,'point':player.sort};
                return value;
            });
                            console.log(vm.playerListe);

            
            for(var i = 0; i < tabSort.length; i++){
                if(tabSort[i].name == vm.sort)
                {
                    vm.logo = tabSort[i].css;   
                }
            }
        });




    }

})();
