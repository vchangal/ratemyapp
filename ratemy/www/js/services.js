(function(){
    angular
    .module('ratemy.services', ['ratemy.config', 'ngCordova', 'ngAnimate',
    'ngResource'])
    .factory('services_ratemy', services_ratemy)
    .factory('service_logout', service_logout)
    .factory('service_checkusername', service_checkusername)
    .factory('ismobile', ismobile)
    .factory('service_islogged', service_islogged)
    .factory('config', configfunc)
    .factory('ryutils', ryutils)
    .factory('shdata', shdata);
   service_checkusername.$inject = ["$http", "config"];
   ryutils.$inject = ["$cordovaToast", "$window", "$log"];
   function service_checkusername($http, config){
        return function(username){
            return $http.post(config.domain + '/test/is_username_available',
                {'username' : username});
        };
    }
    function ismobile(){
        return document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
    }
    function service_islogged(shdata) {
        return function(){
            if(shdata.get('username') && shdata.get('password')){
                return true;
            }
            return false;
        };
    }
    function shdata($window) {
        var ramdata = {
            get: getfunc,
            set: setfunc,
        };
        return ramdata;
        function getfunc(key, dft){
            var obj = JSON.parse($window.localStorage.ratemydata || '{}');
            if(key in obj){
                ramdata[key] = obj[key];
            }
            if(key in ramdata){
                return ramdata[key];
            }
            return dft;
        }
        function setfunc(key, value){
            var obj = JSON.parse($window.localStorage.ratemydata || '{}');
            obj[key] = value;
            $window.localStorage.ratemydata = JSON.stringify(obj);
            ramdata[key] = value;
            return ramdata[key];
        }
    }

    function service_logout($http, $ionicLoading, config){
        return function(username, token, success, error){
            $ionicLoading.show({
                template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner>'
            });
            $http.post(config.domain + '/logout',
                {'username' : username, 'token': token})
                .success(success).error(error).finally(function(){
                    $ionicLoading.hide();
                });

        };

    }
    function services_ratemy($http, $ionicLoading, config,
        $cordovaFileTransfer, $resource, $cordovaDevice, ismobile,
        $document)
    {
        return {
            'hiscores': users,
            'hash': hash,
            'users': users,
            'reports': reports,
            'config': configreq,
            'categories': categories,
            'comments': comments,
            'resources': resources,
            'favorites': favorites,
            'shares': shares,
            'comments_vote': comments_vote,
            'resources_vote': resources_vote,
            'resources_view': resources_view,
            'change_details': change_details,
            'images' : {'get': get_resources,
                'insert': insert_resources},
        };
    function arg_dft(arg, val){
        return (typeof(arg) !== 'undefined' && arg !== null) ? arg : val;
    }
    function get_resources(username, token, resource_id, user, categories,
        offset, limit, success, error)
        {
            offset = arg_dft(offset, 0);
            limit = arg_dft(limit, 1);
            $http.get(config.domain + '/api/v1.0/resources',
                {'username' : username, 'token': token,
                    'categories': categories, 'user':user,
                    'resource_id':resource_id, 'offset': offset,
                    'limit': limit})
                    .success(success).error(error);
        }
        function insert_resources(params,
            filepath, success, error, progress){
                var options = {'params':params};
                document.addEventListener('deviceready', function () {

                    $cordovaFileTransfer.upload(config.domain + '/api/v1.0/resources',
                        filepath, options, true)
                        .then(success, error , progress);
                }, false);
            }
            function change_details(details, filepath, success, error, progress){
                var options = {'params':details};
                document.addEventListener('deviceready', function () {
                    if(filepath){
                        $cordovaFileTransfer.upload(config.domain + '/api/v1.0/users',
                            filepath, options, true)
                            .then(success, error , progress);
                    }else{
                        $http.post(config.domain + '/api/v1.0/users',
                            details).success(success).error(error);
                    }
                }, false);
            }
            function getuuid(){
                if(ismobile){
                    return $cordovaDevice.getUUID();
                }
                return "not a mobile";
            }
            function users(){
                return $resource(config.domain + '/api/v1.0/users/:id', {id:'@id'},
                    {
                        login: {
                            method:"GET",
                            params:{
                                login:true,
                                uuid:getuuid()
                            }
                        },
                        register: {
                            method:"PUT",
                            params:{
                                register:true,
                                uuid:getuuid()
                            }
                        }
                    }
                );
            }
            function comments(){
                return $resource(config.domain + '/api/v1.0/comments', {}, {});
            }
            function reports(){
                return $resource(config.domain + '/api/v1.0/reports', {}, {});
            }
            function comments_vote(){
                return $resource(config.domain + '/api/v1.0/comments/votes', {}, {});
            }
            function resources(){
                return $resource(config.domain + '/api/v1.0/resources', {}, {});
            }
            function categories(){
                return $resource(config.domain + '/api/v1.0/categories', {}, {});
            }
            function resources_vote(){
                return $resource(config.domain + '/api/v1.0/resources/vote', {}, {});
            }
            function resources_view(){
                return $resource(config.domain + '/api/v1.0/resources/view', {}, {});
            }
            function favorites(){
                return $resource(config.domain + '/api/v1.0/favorites', {}, {});
            }
            function shares(){
                return $resource(config.domain + '/api/v1.0/shares', {}, {});
            }
            function configreq(){
                return $resource(config.domain + '/api/v1.0/config', {}, {});
            }

            function hash(data){
                var sha_obj = new jsSHA(data, "TEXT");
                var hashdata = sha_obj.getHash("SHA-512", "HEX");
                return hashdata;
            }
    }
    function ryutils($cordovaToast, $window, $log){
        var _priv = {};
        var data = {
            toast: toast,
            getpict: getpict,
            err: err,
            hideNavBar: hideNavBar,
            showNavBar: showNavBar,
            noHeader: noHeader,
            setExpanded: setExpanded,
            setHeaderFab: setHeaderFab,
            isExpanded: isExpanded,
            isHeaderFabLeft: isHeaderFabLeft,
            isHeaderFabRight: isHeaderFabRight,
            hasHeader: hasHeader,
            hideHeader: hideHeader,
            showHeader: showHeader,
            clearFabs: clearFabs
        };
        return data;
        function err(resp){
            $log.log(resp);
        }
        function hideNavBar() {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        }
        function showNavBar() {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        }
        function noHeader() {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }
        }
        function setExpanded(bool) {
            _priv.isExpanded = bool;
        }
        function isExpanded() {
            return _priv.isExpanded;
        }
        function isHeaderFabLeft() {
            return _priv.hasHeaderFabLeft;
        }
        function isHeaderFabRight() {
            return _priv.hasHeaderFabRight;
        }
        function setHeaderFab(location) {
            var hasHeaderFabLeft = false;
            var hasHeaderFabRight = false;

            switch (location) {
                case 'left':
                    hasHeaderFabLeft = true;
                    break;
                case 'right':
                    hasHeaderFabRight = true;
                    break;
            }

            _priv.hasHeaderFabLeft = hasHeaderFabLeft;
            _priv.hasHeaderFabRight = hasHeaderFabRight;
        }

        function hasHeader() {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (!content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }

        }

        function hideHeader() {
            hideNavBar();
            noHeader();
        }

        function showHeader() {
            showNavBar();
            hasHeader();
        }

        function clearFabs() {
            var fabs = document.getElementsByClassName('button-fab');
            if (fabs.length && fabs.length > 1) {
                fabs[0].remove();
            }
        }
        function toast(msg){
            if(typeof($window.plugins) === 'undefined'){
                $log.log("Toast : " + msg);
            }else{
                $cordovaToast.showShortBottom(msg);
            }
        }
        function getpict(pict){
            var _pict = pict;
            if(_pict === null || typeof _pict === "undefined"){
                _pict = configfunc().default_picture;
            }
            return configfunc().domain + _pict;
        }
    }
    function configfunc(){
        var data = {
            //'domain': 'http://192.168.1.21/ratemy',
            domain: 'http://ratemy.changala.fr',
            default_picture: "/profiles/default_profile.png",
            getcatclasstype: function(type){
                var types = ['ion-camera', 'ion-volume-high', 'ion-videocamera',
                    'ion-document-text'];
                if(type >= 0 && type < types.length){
                    return types[type];
                }
                return types[3];
            }
        };

        return data;
    }
})();
