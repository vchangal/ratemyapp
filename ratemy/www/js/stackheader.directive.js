(function(){
    angular.module('ry.directives')
    .directive('stackHeaders', stackHeaders)
    .directive('stackHeader', stackHeader);
    stackHeader.$inject = [];
    stackHeaders.$inject = [];
    function stackHeader() {
        var index = 0;
        return {
            require: '^^stackHeaders',
            restrict: 'A',
            controller:stackHeaderCtrl,
            link: function(scope, el, attrs, ctrl) {
                ctrl.headers.push(el);
                index = ctrl.headers.length - 1;
                el.css('transition', 'transform .5s ease-in-out');

                scope.$on('$destroy', function() {
                    ctrl.headers.splice(index, 1);
                });
            },
        };
    }
    stackHeaderCtrl.$inject = ["$scope", "$element", "$attrs", "$timeout", "$interval", "$ionicPosition", "$ionicScrollDelegate"];
    function stackHeaderCtrl(scope, el, at, $timeout, $interval, $ionicPosition, $ionicScrollDelegate){
        var vm = this;
    }
    
    
    stackHeadersCtrl.$inject = ["$scope", "$element", "$attrs", "$timeout", "$interval", "$ionicPosition", "$ionicScrollDelegate"];
    function stackHeadersCtrl(scope, el, at, $timeout, $interval, $ionicPosition, $ionicScrollDelegate){
        var vm = this;
        vm.headers = [];
        at.$set('scroll-event-interval', 5);


        el.on('scroll', function(e) {
            for(var i=0; i < vm.headers.length; i++){
                var header = vm.headers[i][0];
                var headerHeight = header.offsetHeight;
                var offsetTop = header.offsetTop;
                var scrollTop = null;
                var header2 = null;
                var offsetTop2 = 0;
                var inter2 = 0;
                if(e.detail){
                    scrollTop = e.detail.scrollTop;
                }else if(e.target){
                    scrollTop = e.target.scrollTop;
                }
                if(i < vm.headers.length - 1){
                    header2 = vm.headers[i+1][0];
                    offsetTop2 = header2.offsetTop;
                    inter2 = Math.max(0,scrollTop - offsetTop2 + headerHeight);
                }
                else{
                    inter2 = 0;
                }
                var inter = scrollTop - offsetTop - inter2;
                var dist = Math.max(0, inter);
                shrink(header, scrollTop - offsetTop, dist, headerHeight);
            }
        });
        function shrink(header, scrollTop, amt, max) {
            var fadeAmt = (max - Math.min(Math.max((scrollTop - amt), 0), max))/max;
            ionic.requestAnimationFrame(function() {
                header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + amt + 'px, 0)';
                for(var i = 0, j = header.children.length; i < j; i++) {
                    header.children[i].style.opacity = fadeAmt;
                }
            });
        }

    }

    function stackHeaders(){

        return {
            restrict: 'A',
            controller:stackHeadersCtrl,
            link: function (scope, el, attrs, ctrl) {
            }
        };

    }

})();
