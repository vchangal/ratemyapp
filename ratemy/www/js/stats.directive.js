(function(){
    angular.module('ry.directives')
    .directive('ryStats', ryStats);

    ryStats.$inject = ["$timeout"];
    function ryStats($timeout) {
        return {
            templateUrl: 'html/ry-stats.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                ryUser: '='
            },
            controller:['$scope', '$element', '$attrs',
                function($scope, $element, $attrs) {
            }],
            link: function (scope, element, attrs, ctrl) {
                var _def = ["",
                    "number of comments",
                    "number of favorite",
                    "number of report",
                    "number of resource",
                    "number of share",
                    "number of view",
                    "number of vote",
                "number of score"];
            var mytimeout = null;
            scope.showDef = function(definition){
                scope.show = true;
                if(mytimeout !== null){
                    $timeout.cancel(mytimeout);
                    mytimeout = null;
                }
                mytimeout = $timeout(function() {
                    scope.show = false;
                    mytimeout = null;
                }, 3500);
                scope.definition = _def[definition];
            };
            }
        };
    }

})();
