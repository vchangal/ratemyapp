(function(){
    angular.module('ry.directives')
    .directive('samepassword', samepassword)
    .directive('alreadytaken',  alreadytaken)
    .directive('ngInfo', ngInfo)
    .factory('ClosePopupService', ClosePopupService);

    alreadytaken.$inject = ["$q", "service_checkusername"];
    function samepassword() {
        return {
            require: 'ngModel',
            scope:{
                samepassword:"="
            },
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.samepassword = function(modelValue, viewValue) {
                    if(modelValue === ""){
                        return false;
                    }
                    return (scope.samepassword == modelValue);
                };
            }
        };
    }

    function alreadytaken($q, service_checkusername) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$asyncValidators.alreadytaken = function(modelValue, viewValue) {
                    if(scope.iscreation){
                        return service_checkusername(modelValue);
                    }else{
                        /* if it is login, do not check username */
                        var def = $q.defer();
                        def.resolve();
                        return def.promise;
                    }
                };
            }
        };
    }
    function ngInfo() {
        return {
            link : function(scope, element, attrs) {
                var img = angular.element('<i class="icon icon-left calm ion-information-circled placeholder-icon"></i>');
                element.prepend(img);
            },
        };
    }
    ClosePopupService.$inject = ['$document', '$ionicPopup', '$timeout'];
    function ClosePopupService($document, $ionicPopup, $timeout){
        var lastPopup;
        var popupservice = {};
        popupservice.register = register;
        return popupservice;
        function register(popup) {
            $timeout(function(){
                var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                if(!element || !popup || !popup.close) return;
                element = element && element.children ? angular.element(element.children()[0]) : null;
                lastPopup = popup;
                var insideClickHandler = function(event){
                    event.stopPropagation();
                };
                var outsideHandler = function() {
                    popup.close();
                };
                element.on('click', insideClickHandler);
                $document.on('click', outsideHandler);
                popup.then(function(){
                    lastPopup = null;
                    element.off('click', insideClickHandler);
                    $document.off('click', outsideHandler);
                });
            });
        }
    }

})();
