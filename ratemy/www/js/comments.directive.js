(function(){
    angular
    .module('ry.directives',
        ['ionic', 'ionic-material', 'ngAnimate', 'angular-underscore',
            'ionicLazyLoad', 'ngCordova', 'ratemy.services', 'ratemy.config',
        'ionMdInput', 'ngMessages'])
    .directive('ryComments', ryComments);
    ryComments.$inject = ["$window", "ryutils", "$ionicModal",
        "services_ratemy", "shdata", '$rootScope', "config", "$ionicPopup",
        "$timeout"];
    ryCommentsCtrl.$inject = ["$scope", "$element", "$attrs", "services_ratemy",
    "shdata", "ryutils", "$timeout", "$rootScope", "$ionicPopup", "config",
    "$ionicModal", "$q"];
    function ryCommentsCtrl(scope, element, attrs, services_ratemy, shdata,
    ryutils, $timeout, rootScope, $ionicPopup, config, $ionicModal, $q){
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.me = shdata.me;
        vm.getcomments = getcomments;
        vm.config = config;
        vm.commentsMore = true;
        vm.edit = false;
        vm.comments = [];
        vm.deletecomment = deletecomment;
        vm.postcomment = postcomment;
        vm.votecomments = votecomments;
        vm.getpict = ryutils.getpict;
        vm._saved = {
            value: false,
            text:"Saved",
            color:"badge-stable",
            handle:null
        };

        activate();
        element.on("click", function(){
            vm.commentsmodal.show();
            scope.$apply();
        });
        /****/
        function activate(){
            vm.mycomment = getmycomment();
            getcomments();
            scope.$watch('vm.mycomment.id', function(newval){
                if(typeof newval === 'undefined' || newval === null){
                    vm.mycomment.css = "black";
                }
                else{
                    vm.mycomment.css = "royal";
                }
            });
            $ionicModal.fromTemplateUrl('html/ry-comments.html', {
                scope: scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                vm.commentsmodal = modal;
                vm.commentsmodal.scope.vm = vm;
            });
            scope.$on('$destroy', function() {
                vm.commentsmodal.remove();
            });
        }
        function deletecomment(){
            var alertPopup = $ionicPopup.confirm({
                title: 'Are you sure?',
                template: 'Once deleted the comment and its notes cannot be retrieved'
            });
            alertPopup.then(function(res) {
                if(res) {
                    var query = {};
                    query.token = token;
                    query.username = username;
                    query.resource_id = vm.slide.id;
                    vm.mycomment.$delete(query, refreshcomments);
                } else {
                }
            });
        }
        function postcomment() {
            vm.mycomment.username = username;
            vm.mycomment.token = token;
            vm.mycomment.resource_id = vm.slide.id;
            vm.mycomment.$save(_postsave);
            vm.edit = false;
        }
        function _postsave(){
            vm.getcomments();
            vm._saved.value = true;
            vm._saved.text = "Saved";
            vm._saved.color = "badge-balanced";
            $timeout.cancel(vm._saved.handle);
            vm._saved.handle = $timeout(function(){
                vm._saved.value = false;
            }, 2000);

        }
        function votecomments(text, note){
            if(note == text.my_note){return;}
            var comments_vote = services_ratemy.comments_vote();
            var vote = new comments_vote();
            vote.token = token;
            vote.username = username;
            vote.user_id = vm.me.id;
            vote.comments_id = text.id;
            vote.note = note;
            vote.$save();
            text.my_note = parseInt(note, 10);
            text.pos_note = parseInt(text.pos_note, 10);
            text.neg_note = parseInt(text.neg_note, 10);
            text.pos_note = Math.max(0, text.pos_note + note);
            text.neg_note = Math.max(0, text.neg_note - note);

        }
        function getmycomment() {
            var query = {};
            query.offset = 0;
            query.limit = 1;
            query.token = token;
            query.username = username;
            query.user_id = vm.me.id;
            query.resource_id = vm.slide.id;
            query.arrayexpected = false;
            return services_ratemy.comments().get(query);
        }
        function refreshcomments(){
            vm.comments.length = 0;
            getcomments();
        }
        function getcomments(){
            var query = {};
            query.offset = vm.comments.length;
            query.limit = 20;
            query.token = token;
            query.username = username;
            query.resource_id = vm.slide.id;
            query.arrayexpected = true;
            return services_ratemy.comments().query(query, _commentsextract);
        }
        function _commentsextract(data){
            if(data.length === 0 && vm.comments.length > 0){
                vm.commentsMore = false;
                ryutils.toast('End of comments');
            }
            else{
                vm.commentsMore = true;
                vm.comments = vm.comments.concat(data);
                var queryvote = {};
                queryvote.offset = 0;
                queryvote.limit = 20;
                queryvote.token = token;
                queryvote.username = username;
                queryvote.user_id = vm.me.id;
                queryvote.arrayexpected = true;
                queryvote.comments_id = rootScope.map(data, function(comment){
                    return comment.id;
                });
                services_ratemy.comments_vote().query(queryvote, function(data){
                    vm.votes = data;
                    rootScope.each(vm.comments, addmynote);
                });
            }
            $timeout(function(){
                scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        function addmynote(comment){
            var predicate = {"comments_id": comment.id};
            var myvote = rootScope.findWhere(vm.votes, predicate) || {};
            comment.my_note = myvote.note || 0;
        }
    }
    function ryComments($window, ryutils, $ionicModal, services_ratemy,
        shdata, $rootScope, config, $ionicPopup, $timeout) {
            var directive = {
                restrict: 'A',
                templateNamespace: 'html',
                scope: {
                    slide: '=',
                    mycomment: '='
                },
                controller: ryCommentsCtrl,
                bindToController: true,
                controllerAs: "vm",
                link: ryCommentsLink
            };
            return directive;
            function ryCommentsLink(scope, element, attrs, ctrl){
            }
    }
})();
