(function(){
    angular.module('ry.directives').directive('ryFilter', ryFilter);

    ryFilterCtrl.$inject = ["$scope", "$element", "$attrs", "services_ratemy"];
    function ryFilterCtrl($scope, $element, $attrs, services_ratemy) {
        var vm = this;
        vm.filterListSelected = [];
        if(vm.filterType == 'unique'){
            vm.uniqueType = true;
            vm.isActive = vm.filterList[0];
            vm.setClass = function(num) {
                vm.isActive = vm.filterList[num];
                vm.filterListSelected = vm.filterList[num].name;
            }
            $scope.$watch('vm.filterListSelected',function(selected){
                vm.filterSelected = selected;
            });
        }
        if(vm.filterType == 'multiple'){
            vm.uniqueType = false;
            vm.selectedFilter = function(num){
                vm.filterListSelected.push(vm.filterList[num]);
                for(var i = 0; i< vm.filterListSelected.length;i++)
                {
                    if(vm.filterListSelected[i].show == false)
                    {
                        vm.filterListSelected.splice(i,1);
                    }
                }

            }


            vm.filterSelected = vm.filterListSelected;

        }

    }
    function ryFilter() {
        return {
            templateUrl: 'html/ry-filter.html',
            restrict: 'E',
            templateNamespace: 'html',
            bindToController: true,
            controllerAs: "vm",
            scope: {
                filterType:'@',
                filterList:'=',
                filterSelected:'='
            },
            controller:ryFilterCtrl,
            link: function (scope, element, attrs, ctrl) {

            }
        };
    }
})();
