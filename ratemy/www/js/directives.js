(function(){
    var app = angular.module('ratemy.directives', ['ionic', 'ngAnimate',
                                                   'angular-underscore',
                                                   'ngCordova',
                                                   'ratemy.services', 'ratemy.config']);



    app.directive('ryRecordSound', function ($interval, $timeout, $cordovaMedia,
                                              $cordovaFile, $q) {
        return {
            templateUrl: 'html/ry-record-sound.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                ryResource :'='
            },
            controller:['$scope', '$element', '$attrs', '$rootScope',
                        function($scope, $element, $attrs, $rootScope) {
                            $scope.currenttime = 0;
                            $scope.maxtime = 5;
                            $scope.isplaying = false;
                            $scope.isrecording = false;
                            $scope.recorddisplay = {false:'ion-mic-a balanced',
                                                    true:'ion-stop assertive'};
                            $scope.playdisplay = {false:'ion-play balanced',
                                                  true:'ion-stop assertive'};
                            $scope.updaterecordtime = function(_time){
                                $scope.currenttime = Math.round(_time*10)/10;
                            };
                            var tmp;
                            var tmp2;
                            var saveDuration = $scope.maxtime;
                            $scope.asyncGreet = function() {
                                var defer = $q.defer();
                                var _time = 0.0;
                                var srcaudio = cordova.file.externalDataDirectory+"/ratemy.amr";
                                var audio = $cordovaMedia.newMedia(srcaudio);
                                tmp = $interval(function(){
                                    _time += 0.1;
                                    defer.notify(_time);
                                    if(_time >= $scope.maxtime || !$scope.isrecording){
                                        $scope.isrecording = false;
                                        audio.stopRecord();
                                        audio.release();
                                        saveDuration = Math.round(_time*100)/100;
                                        $scope.maxtime = saveDuration;
                                        defer.resolve(srcaudio);
                                        $interval.cancel(tmp);
                                    }
                                }, 100);
                                $timeout(function(){
                                    $scope.isrecording = true;
                                    audio.startRecord();
                                });
                                return defer.promise;
                            };
                            $scope.asyncPlay = function() {
                                var defer = $q.defer();
                                var _time = 0.0;
                                var srcaudio = cordova.file.externalDataDirectory+"/ratemy.amr";
                                var audio = $cordovaMedia.newMedia(srcaudio);
                                tmp = $interval(function(){
                                    _time += 0.1;
                                    defer.notify(_time);
                                    $scope.maxtime = saveDuration;
                                    if(_time >= saveDuration || !$scope.isplaying){
                                        $scope.isplaying = false;
                                        audio.stop();
                                        audio.release();
                                        defer.resolve(srcaudio);
                                        $interval.cancel(tmp);
                                    }
                                }, 100);
                                $timeout(function(){
                                    $scope.isplaying = true;
                                    audio.play();
                                });
                                return defer.promise;
                            };
                            $scope.play = function () {
                                if(!$scope.isplaying){
                                    $scope.asyncPlay().then(function(filepath){
                                        $scope.currenttime = 0;
                                    }, null, $scope.updaterecordtime);
                                }
                                else{
                                    $scope.isplaying = false;
                                }
                            };
                            $scope.record = function () {
                                if(!$scope.isrecording){
                                    $scope.asyncGreet().then(function(filepath){
                                        $scope.currenttime = 0;
                                        $scope.ryResource = filepath;
                                    }, null, $scope.updaterecordtime);
                                }
                                else{
                                    $scope.isrecording = false;
                                }
                            };
                        }],
            link: function (scope, iElement, iAttrs, ctrl) {
            }
        };
    });
})();
