(function(){
    angular
        .module('ry.login.controllers',
                ['ionic', 'ratemy.services',
                 'ngMessages', 'ngAnimate', 'ui.router',
                 'ngCordova', 'angular-underscore'])

        .controller("loginCtrl", loginCtrl);

    function loginCtrl ($scope, $ionicPopup, $state,
                         services_ratemy, config,
                         shdata, ryutils, $q){
        var vm = this;

        vm.username = shdata.get('username', "");
        vm.pass1 = shdata.get("password", "");
        vm.submit = submit;

        activate();

        /***/
        function submit() {
            if(vm.iscreation){
                return register();
            }
            return login();
        }
        function errlogin(err){
            ryutils.toast('Login failed, check your credentials!');
        }
        function postlogin(data){
            shdata.token = data.token;
            shdata.set('username', vm.username);
            shdata.set('password', vm.pass1);
            var user = services_ratemy.users().get({username:vm.username,
                token:data.token,
                id:data.id},function(remainingdata){
                    shdata.me = $scope.extend(data, remainingdata);
                });
            var config = services_ratemy.config().query({username:vm.username,
                token:data.token},
                function(configdata){
                    $scope.forEach(configdata, function(row){
                        config[row.name] = JSON.parse(row.value);
                    });
                });
            $q.all([user.$promise, config.$promise]).then(function(){
                $state.go('tabs.menu');
            });
        }
        function login() {
            // Start showing the progress
            // Do the call to a service using $http or directly do the call here
            var hashed = services_ratemy.hash(vm.pass1);
            return services_ratemy.users().login({username:vm.username, pass1:hashed},
                postlogin, errlogin);
        }
        function register() {
            var hashed1 = services_ratemy.hash(vm.pass1);
            var hashed2 = services_ratemy.hash(vm.pass2);
            return services_ratemy.users().register({username:vm.username,
                pass1:hashed1,
                pass2:hashed2},
                postlogin, function(data){
                    $ionicPopup.alert({
                        title: 'Registration failed!',
                        template: 'Please check your credentials!'
                    });
                });
        }
        function activate(){
            if(vm.username != '' && vm.pass1 != ''){
                login();
            }
        }
    }

})();
