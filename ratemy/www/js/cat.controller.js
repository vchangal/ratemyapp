(function(){
    angular
    .module('ry.cat.controllers',
        ['ionic', 'ionic-material', 'ratemy.services',
            'ngMessages', 'ngAnimate', 'ui.router', 'ionicLazyLoad',
        'ngCordova', 'angular-underscore', 'ionic.ion.imageCacheFactory'])
    .controller("catCtrl", catCtrl);

    function catCtrl($scope, $stateParams, $cordovaCapture,
                      services_ratemy, shdata,
                      ryutils, ionicMaterialInk, ionicMaterialMotion,
                      $ionicSlideBoxDelegate, $cordovaCamera,
                      $ionicScrollDelegate, $ionicPopup,
                      $timeout, $ionicModal, $ionicActionSheet,
                      $cordovaMedia, $ionicPlatform, $ImageCacheFactory,
                      $cordovaFile, $interval, $sce, $window, slides, config){
        var vm = this;
        vm.cats = shdata.categories;
        var username = shdata.username;
        var token = shdata.token;
        vm.type = $stateParams.type;
        vm.config = config;
        vm.slides = slides;
        vm.reachEnd = reachEnd;
        vm.resourceMore = true;
        vm.filterpopup = false;
        vm.getactiveslides = getactiveslides;

        activate();

        /*******/
        function activate(){
            ryutils.showHeader();
            ryutils.clearFabs();
            ryutils.setExpanded(true);
            ryutils.setHeaderFab(false);
            ionicMaterialInk.displayEffect();
            $scope.each(vm.cats, function(cat){
                cat.show = true;
            });
            $scope.$watch("vm.slider", function(newval, old){
                if(newval != old){
                    vm.slider.on('ReachEnd', reachEnd);
                }

            });
        }
        function getactiveslides(slides){
            var tmp = $scope.filter(vm.cats,
                                    function(cat){
                                        return cat.show;
                                    });
            vm.catidarray = $scope.map(tmp, function(cat){return cat.id;});
            tmp = $scope.filter(slides,
                function(slide){
                    return $scope.contains(vm.catidarray,slide.category);
                });
            return tmp;
        }
        function extractImages(data){
            var imgs = $scope.map(data, function(pic){return vm.config.domain + pic.path;});
            //$ImageCacheFactory.Cache(imgs).finally(function(){
                vm.slides = vm.slides.concat(data);
                $ionicScrollDelegate.resize();
                if(data.length <= 0 || vm.slides.length <= 0){
                    ryutils.toast('End of the list!');
                    vm.resourceMore = false;
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
                ionicMaterialInk.displayEffect();
           // });
        }

        function reachEnd(){
            var index = vm.slides.length;
            var query = {};
            query.username = username;
            query.token = token;
            if($stateParams.cat){
                var catselected = shdata.categories[$stateParams.cat];
                query["categories"] = catselected.id;
            }
            query.offset = index + 1;
            query.limit = 6;
            query.type = vm.type;
            return services_ratemy.resources().query(query,
                extractImages,ryutils.err);
        }

    }
})();
