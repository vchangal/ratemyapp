(function(){
    angular
    .module('ry.global.controllers', ['ionic', 'ratemy.services',
            'ngMessages', 'ngAnimate', 'ui.router',
            'ngCordova', 'angular-underscore'])
    .controller("globalCtrl", globalCtrl);

    globalCtrl.$inject = ["$scope", "$state", "$log", "shdata", "config"]; 

    function globalCtrl($scope, $state, $log, shdata,
                        config){
        var vm = this;
        vm.config = config;
        vm._err = function(err){
            $log.info(err);
        };
    }

})();
