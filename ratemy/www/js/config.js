var app = angular.module('ratemy.config', []);

app.constant('configstatic', {
    //'domain': 'http://192.168.1.21/ratemy',
    'domain': 'http://ratemy.changala.fr',
    'default_picture': "/profiles/default_profile.png",
    'getcatclasstype': function(type){
        var types = ['ion-camera', 'ion-volume-high', 'ion-videocamera',
            'ion-document-text'];
        if(type >= 0 && type < types.length){
            return types[type];
        }
        return types[3];
    },

});
