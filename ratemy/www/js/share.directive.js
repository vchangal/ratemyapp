(function(){
    angular.module('ry.directives')
    .directive('ryShare', ryShare);

    ryShareCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', "$timeout"];
    function ryShareCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.addOrDeleteShares = addOrDeleteShares;

        /** init **/
       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       element.on("click", function(){
           vm.addOrDeleteShares();
           scope.$apply();
       });

        function addOrDeleteShares(){
            if(angular.isUndefined(vm.myshare.id) || vm.myshare.id == 0){
                vm.myshare.resource_id = vm.slide.id;
                vm.myshare.username = username;
                vm.myshare.token = token;
                vm.myshare.$save();
            }else{
                vm.myshare.$delete({username:username,
                    token:token,
                    resource_id:vm.slide.id},function(data){
                        vm.myshare = data;
                    });
            }
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myshare = services_ratemy.shares().get(query);
            scope.$watch('vm.myshare.id', function(newval){
                if(angular.isUndefined(newval) || newval == 0){
                    vm.myshare.css = "dark";
                }else{
                    vm.myshare.css = "positive";
                }

            });
        }
    }
    ryShare.$inject = ['services_ratemy', 'shdata', '$ionicPopup',
    'config'];
    function ryShare(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button icon button-icon icon-left ion-share"',
                'ng-class="vm.myshare.css"',
                'ng-click="vm.share()">',
                '</a>'
            ].join(''),*/
            restrict: 'AE',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myshare: '='
            },
            controller:ryShareCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();
