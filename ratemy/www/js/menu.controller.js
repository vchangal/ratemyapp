(function(){
    angular.module('ry.menu.controllers', [
        'ionic', 'ratemy.services', 'ngMessages',
        'ngAnimate', 'ui.router', 'ionic-material',
        'ngCordova', 'angular-underscore'])
    .controller("menuCtrl", menuCtrl);
    menuCtrl.$inject = ["$scope", "$state", "$log",
        "ionicMaterialInk", "ionicMaterialMotion",
    "shdata", "config", "categories", "$timeout", "ryutils"];
    function menuCtrl($scope, $state, $log, ionicMaterialInk,
        ionicMaterialMotion, shdata, config, categories, $timeout, ryutils){
        var vm = this;
        vm.config = config;
        vm.categories = categories;
        vm.getcatstyle = getcatstyle;

        ryutils.showHeader();
        ryutils.clearFabs();
        ryutils.setExpanded(true);
        ryutils.setHeaderFab(false);
        ionicMaterialInk.displayEffect();
        $timeout(function() {
            ionicMaterialMotion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 200);
        function getcatstyle(cat){
            var style = {
                'background-image':'url('+ vm.config.domain + cat.image+')',
                'background-size':'cover',
                'background-position': 'center center'
            };
            return style;
        }
    }
})();
