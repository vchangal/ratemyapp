(function(){
    angular
        .module('ry.directives')
        .directive("ryResource", ryResource);
    resourceCtrl.$inject = ["$scope", "$state", "shdata",
        "services_ratemy", "config",
        "ryutils", "$timeout", "ionicMaterialMotion", "ionicMaterialInk"];
    ryResource.$inject = ["services_ratemy", "shdata", "config"];
    function ryResource(services_ratemy, shdata, config) {
        var directive = {
            templateUrl: 'html/resource.html',
            restrict: 'E',
            templateNamespace: 'html',
            scope: {
                slide: '='
            },
            controller:resourceCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {
            }
        };
        return directive;
    }
    function resourceCtrl(scope, $state, shdata,
                          services_ratemy, config, ryutils, $timeout,
                         ionicMaterialMotion, ionicMaterialInk){
       var vm = this;
       var username = shdata.username;
       var token = shdata.token;
       var me = shdata.me;
       vm.config = config;
       vm.showdescr = false;
       vm.getpict = ryutils.getpict;
       vm.pict = {
           'background-image': "url('" + vm.getpict(vm.slide.picture) +"')"
       };
       vm.getslidestyle = getslidestyle;

       scope.$watch("vm.slide", function(newval, old){
           if(newval != old){
               activate();
           }
       });
       /******/
       function activate(){
           var query = {
               username : username,
               token : token,
               user_id : me.id,
               resource_id : vm.slide.id,
               arrayexpected:false
           };
           vm.view = services_ratemy.resources_view().get(query,_postsave);
           // Ajout d'une vue
           scope.$on('ryCommentsLoaded', function(){
               scope.$emit('ryResourceHasChanged');
           });
           ionicMaterialInk.displayEffect();
       }
       function getslidestyle(slide){
           var style = {
               height: "300px",
               'background-size': 'cover',
               'background-position': 'center center'
               //'background-image': "url('" + vm.config.domain + slide.path + "')"
           };
           return style;
       }
       function _postsave(){
           vm.view.username = username;
           vm.view.user_id = me.id;
           vm.view.token = token;
           vm.view.resource_id = vm.slide.id;
           vm.view.$save();
       }
    }
})();
