(function(){
    angular.module('ry.directives')
        .directive('ryReport', ryReport);

    ryReportsCtrl.$inject = ['$scope', '$element', '$attrs',
                             'shdata', 'config', 'services_ratemy', "$ionicPopup", "$timeout"];
    function ryReportsCtrl(scope, element, $attrs, shdata, config,
                            services_ratemy, $ionicPopup, $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        vm.config = config;
        vm.me = shdata.me;
        vm.showPopupReport = showPopupReport;
        vm.myreport = {};
        vm.myreport.css = "assertive";
        vm.reportReason = [{id: 1, name: "Insult"},{id: 2, name: "Explicit content"},{id: 3, name: "Offensive content"}];
        vm.reason = [];

        scope.$watch('vm.reason',function(reportReason){
            for(var i =0; i < vm.reportReason.length;i++)
            {
                if(vm.reportReason[i].name == reportReason)
                {
                    vm.idReason = vm.reportReason[i].id;
                }
            }
        });

        function report(){
            console.log("commentaire : " + vm.commentaire);
            console.log("id raison : " + vm.idReason);
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                comments:vm.commentaire,
                reason:vm.idReason
            }
            services_ratemy.reports().query(query);
        }

        /** init **/
        scope.$watch("vm.slide", function(newval, old){
            if(newval != old){
                activate();
            }
        });
        element.on("click", function(){
            vm.showPopupReport();
            scope.$apply();
        });
        /*
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };

            vm.myreport = services_ratemy.reports().get(query);
        } */
        function showPopupReport(){
            var reportPopup = $ionicPopup.confirm({
                title: 'Report',
                templateUrl: 'html/add_report_resource.html',
                scope:scope,
                buttons:[{ text: 'Cancel',
                          onTap: function(e) {
                          }
                         },
                         {
                             text: '<b>Submit</b>',
                             type: 'button-positive',
                             onTap: function(e) {
                                 // scope.reportMessage.$save();
                                 console.log("Report submitted");
                                 report();
                             }
                         },
                        ]
            });
        };
    }
    ryReport.$inject = ['services_ratemy', 'shdata', '$ionicPopup', 'config'];
    function ryReport(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            /*template: [
                '<a class="button button-icon icon icon-left ion-alert"',
                'ng-class="vm.myreport.css"',
                'ng-click="vm.showPopupReport()">',
                '</a>'
            ].join(''),*/
            restrict: 'EA',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myreport: '='
            },
            controller:ryReportsCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();
