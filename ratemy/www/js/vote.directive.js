(function(){
    angular.module('ry.directives')
    .directive('ryVote', ryVote);

    ryVoteCtrl.$inject = ['$scope', '$element', '$attrs',
    'shdata', 'config', 'services_ratemy', '$ionicPopup', '$timeout',
    'ClosePopupService', '$timeout'];
    function ryVoteCtrl(scope, element, $attrs, shdata, config,
    services_ratemy, $ionicPopup, $timeout, ClosePopupService,
    $timeout) {
        var vm = this;
        var username = shdata.get('username');
        var token = shdata.token;
        var popupdetails = {
            templateUrl: 'html/ry-vote.html',
            title: 'Rate it!',
            subTitle: '',
            scope: scope,
            buttons: [ ]
        };
        vm.config = config;
        vm.me = shdata.me;
        vm.prevnote = 0;
        vm.stars = [1, 2, 3, 4, 5];
        vm.votedclass = [
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline",
            "ion-android-star-outline"
        ];
        vm.vote = vote;
        vm.openVote = openVote;

        /** init **/
        activate();
        element.on("click", function(){
            openVote();
            scope.$apply();
        });

        function openVote(){
            vm.votepopup = $ionicPopup.show(popupdetails);
            ClosePopupService.register(vm.votepopup);
        }
        function updateStars($index){
            var itr = 0;
            for (itr=0; itr < vm.stars.length; itr++) {
                if(itr <= $index){
                    vm.votedclass[itr] = "ion-android-star";
                }
                else{
                    vm.votedclass[itr] = "ion-android-star-outline";
                }
            }
        }
        function vote($index) {
            if(vm.prevnote != $index){
                vm.myvote.username = username;
                vm.myvote.token = token;
                vm.myvote.resource_id = vm.slide.id;
                vm.myvote.note = $index + 1;
                vm.myvote.$save(function(){
                    vm.prevnote = $index;
                });
                updateStars($index);
                updateNote(vm.myvote.note);
            }
            $timeout(function(){
                vm.votepopup.close();
            }, 500);
        }
        function activate(){
            var query = {
                username:username,
                token:token,
                resource_id:vm.slide.id,
                arrayexpected:false,
                user_id:vm.me.id,
                offset : 0,
                limit : 1
            };
            vm.myvote = services_ratemy.resources_vote().get(query, function(data){
                updateNote(data.note);
                vm.myvote.username = username;
                vm.prevnote = data.note - 1;
                updateStars(vm.prevnote);
            });
        }
        function updateNote(note){
            if(typeof note === "undefined"){
                vm.myvote.css = "black";
                vm.prevnote = 0;
            }else{
                vm.myvote.css = "royal";
                vm.prevnote = note - 1;
            }
        }
    }
    ryVote.$inject = ['services_ratemy', 'shdata', '$ionicPopup', 'config'];
    function ryVote(services_ratemy, shdata, $ionicPopup, config) {
        var directive = {
            restrict: 'A',
            templateNamespace: 'html',
            scope: {
                slide: '=',
                myvote: '='
            },
            controller:ryVoteCtrl,
            bindToController: true,
            controllerAs: "vm",
            link: function (scope, element, attrs, ctrl) {

            }
        };
        return directive;
    }

})();
